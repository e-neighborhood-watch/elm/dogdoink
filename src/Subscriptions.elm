module Subscriptions
  exposing
    ( subscriptions
    )


import Death.Subscriptions
  as Death
import Level.Subscriptions
  as Level
import LevelSelect.Subscriptions
  as LevelSelect
import Model.Type
  as Model
  exposing
    ( Model
    )
import Message.Type
  exposing
    ( Message (..)
    )
import Prize.Subscriptions
  as Prize


subscriptions : Model -> Sub Message
subscriptions { model, settings } =
  case
    model
  of
    Model.Level _ _ level ->
      level
        |> Level.subscriptions settings
        |> Sub.map Level

    Model.LevelSelect options ->
      options
        |> LevelSelect.subscriptions settings
        |> Sub.map LevelSelect

    Model.Prize _ _ prizeModel ->
      prizeModel
        |> Prize.subscriptions settings
        |> Sub.map Prize

    Model.Settings _ _ ->
      Sub.none
      
    Model.HelpScreen _ ->
      Sub.none

    Model.GeneratingLevel _ _ _ ->
      Sub.none

    Model.Death _ _ levelModel ->
      levelModel
      |> Death.subscriptions settings
      |> Sub.map Death
