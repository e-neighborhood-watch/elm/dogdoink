module View
  exposing
    ( view
    )


import Browser
import Css
import Css.Global
  as Css
import Html
  as ElmHtml
import Html.Styled
  as Html
  exposing
    ( Html
    )
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Defs
  exposing
    ( defs
    )
import Data.Text.Type
  as Text
import Death.View
  as Death
import Document.Type
  exposing
    ( Document
    )
import HelpScreen.View
  as HelpScreen
import GeneratingLevel.View
  as GeneratingLevel
import Level.View
  as Level
import LevelSelect.View
  as LevelSelect
import Model.Type
  exposing
    ( Model
    , InnerModel (..)
    )
import Message.Type
  as Message
  exposing
    ( Message
    )
import Prize.View
  as Prize
import Settings.Color.Theme
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )
import Settings.View
  as Settings
import Settings.Wheel.View
  as SettingsWheel
import Util.Box.From
  as Box


mapHtml : (a -> Message) -> List Css.Snippet -> List (Html Message) -> Document a -> Browser.Document Message
mapHtml f globalStyles extras { title, viewBox, body } =
  { title =
    title
  , body =
    [ Css.global
      globalStyles
      |> Html.toUnstyled
    , Html.div
      []
      (List.map (Html.map f) body ++ extras)
      |> Html.toUnstyled
    ]
  }


mapDocument : (a -> Message) -> List Css.Snippet -> List (Svg Message) -> Document a -> Browser.Document Message
mapDocument f globalStyles extras { title, viewBox, body } =
  { title =
    title
  , body =
    [ Css.global
      globalStyles
      |> Html.toUnstyled
    , Svg.svg
      [ SvgAttr.viewBox
        ( String.fromFloat viewBox.min.x
        ++ " "
        ++ String.fromFloat viewBox.min.y
        ++ " "
        ++ String.fromFloat viewBox.max.x
        ++ " "
        ++ String.fromFloat viewBox.max.y
        )
      , SvgAttr.preserveAspectRatio "xMidYMid"
      , SvgAttr.css
        [ Css.position Css.absolute
        , Css.top Css.zero
        , Css.left Css.zero
        , Css.width (Css.pct 100)
        , Css.height (Css.pct 100)
        ]
      ]
      (defs :: List.map (Svg.map f) body ++ extras)
      |> Html.toUnstyled
    ]
  }


viewScore : Settings -> Int -> Svg a
viewScore settings score =
  Svg.text_
    [ SvgAttr.css
      [ Css.property "dominant-baseline" "auto"
      , Css.property "text-anchor" "start"
      ]
    , SvgAttr.x "0"
    , SvgAttr.y "-0.1"
    , Font.toCss Font.Text settings.fontSize
    , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
    , SvgAttr.fontWeight "bold"
    ]
    [ Svg.text (Language.fromText settings.language Text.Score ++ String.fromInt score)
    ]


view : Model -> Browser.Document Message
view { model, settings } =
  let
    globalStyles theme =
      [ Css.body
        [ Css.backgroundColor
          ( Color.Background
          |> Theme.toHex theme
          |> Css.hex
          )
        ]
      ]
  in
    case
      model
    of
      Level score levelBound levelModel ->
        case
          Level.view settings (Box.fromPosition levelBound) levelModel
        of
          doc ->
            mapDocument
              ( always Message.Noop )
              ( globalStyles settings.theme )
              [ SettingsWheel.view doc.viewBox settings.theme
              , viewScore settings score
              ]
              doc

      LevelSelect levelSelectModel ->
        case
          LevelSelect.view settings levelSelectModel
        of
          doc ->
            mapDocument
              Message.LevelSelect
              ( globalStyles settings.theme )
              [ SettingsWheel.view doc.viewBox settings.theme
              ]
              doc

      Model.Type.Settings _ settingsModel ->
        settingsModel
          |> Settings.view
          |> mapDocument
            Message.Settings
            ( globalStyles settingsModel.settings.theme )
            [
            ]

      Prize score levelBound prizeModel ->
        case
          Prize.view settings (Box.fromPosition levelBound) prizeModel
        of
          doc ->
            mapDocument
              Message.Prize
              ( globalStyles settings.theme )
              [ SettingsWheel.view doc.viewBox settings.theme
              , viewScore settings score
              ]
              doc

      GeneratingLevel score _ _ ->
        case
          GeneratingLevel.view settings
        of
          doc ->
            mapDocument
              identity
              ( globalStyles settings.theme )
              [ SettingsWheel.view doc.viewBox settings.theme
              , viewScore settings score
              ]
              doc

      Death score levelBound levelModel ->
        Death.view settings (Box.fromPosition levelBound) levelModel
          |>
            mapDocument
              Message.Death
              ( globalStyles settings.theme )
              [ viewScore settings score
              ]
              
      HelpScreen helpModel ->
        mapHtml
          Message.Help
          ( globalStyles settings.theme )
          []
          ( HelpScreen.view helpModel settings )
