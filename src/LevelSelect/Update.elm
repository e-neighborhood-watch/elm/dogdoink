module LevelSelect.Update
  exposing
    ( update
    )


import Level.Type
  as Level
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )


import LevelSelect.Model.Option.Type
  as Option
import LevelSelect.Update.Result.Type
  as Update


update : Message -> Model -> Update.Result
update msg { selected } =
  case
    msg
  of
    Select ->
      Update.Selected

    Previous ->
      case
        selected
      of
        Option.StartGame ->
          Update.NoChange

        Option.Help ->
          Update.Updated
            { selected =
              Option.StartGame
            }

    Next ->
      case
        selected
      of
        Option.StartGame ->
          Update.Updated
            { selected =
              Option.Help
            }

        Option.Help ->
          Update.NoChange
          
    StartGame ->
      Update.StartGameResult
    
    ViewHelp ->
      Update.ViewHelpResult
