module LevelSelect.Model.Init
  exposing
    ( init
    )


import LevelSelect.Model.Option.Type
  exposing
    ( Option (..)
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )


init : Model
init =
  { selected =
    StartGame
  }
