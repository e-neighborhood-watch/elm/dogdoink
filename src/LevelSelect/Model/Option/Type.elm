module LevelSelect.Model.Option.Type
  exposing
    ( Option (..)
    )


import Level.Status.Type
  as Status
  exposing
    ( WithStatus
    )
import Level.Type
  as Level
  exposing
    ( Level
    )


type Option
  = StartGame
  | Help
