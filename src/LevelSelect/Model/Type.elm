module LevelSelect.Model.Type
  exposing
    ( Model
    )


import LevelSelect.Model.Option.Type
  as Option
  exposing
    ( Option
    )


type alias Model =
  { selected :
    Option
  }
