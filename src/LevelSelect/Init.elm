module LevelSelect.Init
  exposing
    ( init
    )


import Level.Status.Type
  as Status
import Level.Type
  exposing
    ( Level
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import LevelSelect.Model.Option.Type
  exposing
    ( Option (..)
    )


init : Level -> List Level -> Model
init current remaining =
  { selected =
    StartGame
  }
