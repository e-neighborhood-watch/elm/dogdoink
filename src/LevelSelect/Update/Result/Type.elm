module LevelSelect.Update.Result.Type
  exposing
    ( Result (..)
    )


import Level.Type
  exposing
    ( Level
    )


import LevelSelect.Model.Type
  exposing
    ( Model
    )


type Result
  = NoChange
  | Updated Model
  | Selected
  | StartGameResult
  | ViewHelpResult