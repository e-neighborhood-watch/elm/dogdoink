module LevelSelect.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
import Svg.Styled.Attributes
  as SvgAttr
import Svg.Styled.Events
  as Svg


import Document.Type
  exposing
    ( Document
    )


import Data.Color.Theme.Type
  as Color
import Data.Font.Size
  as Font
import Data.Text.Type
  as Text
import LevelSelect.Message.Type
  exposing
    ( Message (..)
    )
import LevelSelect.Model.Option.Type
  as Option
import LevelSelect.Model.Type
  exposing
    ( Model
    )
import Settings.Color.Theme
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )


view : Settings -> Model -> Document Message
view settings { selected } =
  let
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          8.2
        , y =
          8.2
        }
      }

    ( ( startGameColor, startGameWeight ), ( helpColor, helpWeight ) ) =
      case
        selected
      of
        Option.StartGame ->
          ( ( Color.Foreground
            , "bold"
            )
          , ( Color.Accent
            , "normal"
            )
          )

        Option.Help ->
          ( ( Color.Accent
            , "normal"
            )
          , ( Color.Foreground
            , "bold"
            )
          )

    rawTextSize =
      Font.rawSize Font.Title settings.fontSize
  in
    { title =
      Language.fromText settings.language Text.LevelSelectTitle
    , viewBox =
      viewBox
    , body =
      [ Svg.text_
        [ SvgAttr.css
          [ Css.property "dominant-baseline" "central"
          , Css.property "text-anchor" "middle"
          ]
        , Font.toCss Font.Title settings.fontSize
        , startGameColor
          |> Theme.toHex settings.theme
          |> SvgAttr.fill 
        , (viewBox.min.x + viewBox.max.x) / 2
          |> String.fromFloat
          |> SvgAttr.x
        , (viewBox.min.y + viewBox.max.y - rawTextSize) / 2
          |> String.fromFloat
          |> SvgAttr.y
        , SvgAttr.fontWeight startGameWeight
        , Svg.onClick StartGame
        ]
        [ Language.fromText settings.language Text.LevelSelectStartGame
          |> Svg.text
        ]
      , Svg.text_
        [ SvgAttr.css
          [ Css.property "dominant-baseline" "central"
          , Css.property "text-anchor" "middle"
          ]
        , Font.toCss Font.Title settings.fontSize
        , helpColor
          |> Theme.toHex settings.theme
          |> SvgAttr.fill 
        , (viewBox.min.x + viewBox.max.x) / 2
          |> String.fromFloat
          |> SvgAttr.x
        , (viewBox.min.y + viewBox.max.y + rawTextSize) / 2
          |> String.fromFloat
          |> SvgAttr.y
        , SvgAttr.fontWeight helpWeight
        , Svg.onClick ViewHelp
        ]
        [ Language.fromText settings.language Text.LevelSelectHelp
          |> Svg.text
        ]
      ]
    }
