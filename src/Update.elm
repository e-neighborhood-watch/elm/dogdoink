module Update
  exposing
    ( update
    )


import Random


import Death.Update
  as Death
import Death.Update.Result.Type
  as Death
import HelpScreen.Message.Type
  as HelpScreen
import HelpScreen.Model.Type
  as HelpScreen
import Level.Generator
  as Level
import Level.Model.Init
  as Level
import Level.Player.Health.Alive
  as PlayerHealth
import Level.Player.Health.Cell.Default
  as Cell
import Level.Player.Health.Cell.Type
  as Cell
import Level.Player.Health.Uniform
  as PlayerHealth
import Level.Size
  as Level
import Level.Status.Type
  as Status
import Level.Type
  as Level
import Level.Update
  as Level
import Level.Update.Result.Type
  as Level
import LevelSelect.Model.Init
  as LevelSelect
import LevelSelect.Model.Option.Type
  as LevelSelect
import LevelSelect.Model.Type
  as LevelSelect
import LevelSelect.Update
  as LevelSelect
import LevelSelect.Update.Result.Type
  as LevelSelect
import Model.Type
  as Model
  exposing
    ( Model
    )
import Model.Suspended.Type
  as SuspendedModel
import Message.Type as Message
  exposing
    ( Message
    )
import Prize.CursorState.Type
  as Prize
import Prize.Mode.Type
  as PrizeMode
import Prize.Update
  as Prize
import Prize.Update.Result.Type
  as Prize
import Settings.Update
  as Settings
import Settings.Update.Result.Type
  as Settings
import Util.Box.From
  as Box
import Util.Position.Type
  exposing
    ( Position
    )


update : Message -> Model -> ( Model, Cmd Message )
update message prevModel =
  case
    ( message, prevModel.model )
  of
    -- Don't open the settings twice
    ( Message.OpenSettings, Model.Settings _ _ ) ->
      ( prevModel
      , Cmd.none
      )

    ( Message.OpenSettings, Model.Level score levelBound levelModel ) ->
      ( { prevModel
        | model =
          Model.Settings
            (SuspendedModel.Level score levelBound levelModel)
            { settings =
              prevModel.settings
            }
        }
      , Cmd.none
      )

    ( Message.OpenSettings, Model.Prize score levelBound prizeModel ) ->
      ( { prevModel
        | model =
          Model.Settings
            ( SuspendedModel.Prize score levelBound prizeModel )
            { settings =
              prevModel.settings
            }
        }
      , Cmd.none
      )

    ( Message.OpenSettings, Model.LevelSelect option ) ->
      ( { prevModel
        | model =
          Model.Settings
            ( SuspendedModel.LevelSelect option )
            { settings =
              prevModel.settings
            }
        }
      , Cmd.none
      )

    ( Message.Settings settingsMessage, Model.Settings model settingsModel ) ->
      case
        Settings.update settingsMessage settingsModel
      of
        Settings.NoChange ->
          ( prevModel
          , Cmd.none
          )

        Settings.ChangeModel newSettingsModel ->
          ( { prevModel
            | model =
              Model.Settings
                model
                newSettingsModel
            }
          , Cmd.none
          )

        Settings.Cancel ->
          ( { prevModel
            | model =
              case
                model
              of
                SuspendedModel.LevelSelect option ->
                  Model.LevelSelect option
                SuspendedModel.Level score levelBound levelModel ->
                  Model.Level score levelBound levelModel
                SuspendedModel.Prize score levelBound prizeModel ->
                  Model.Prize score levelBound prizeModel
            }
          , Cmd.none
          )

        Settings.SaveAndExit ->
          ( { prevModel
            | model =
              case
                model
              of
                SuspendedModel.LevelSelect option ->
                  Model.LevelSelect option
                SuspendedModel.Level score levelBound levelModel ->
                  Model.Level score levelBound levelModel
                SuspendedModel.Prize score levelBound prizeModel ->
                  Model.Prize score levelBound prizeModel
            , settings =
              settingsModel.settings
            }
          , Cmd.none
          )

    ( Message.LevelSelect levelSelectMessage, Model.LevelSelect option ) ->
      case
        LevelSelect.update levelSelectMessage option
      of
        LevelSelect.NoChange ->
          ( prevModel
          , Cmd.none
          )

        LevelSelect.Updated newOption ->
          ( { prevModel
            | model =
              Model.LevelSelect newOption
            }
          , Cmd.none
          )

        LevelSelect.Selected ->
          case
            option.selected
          of
            LevelSelect.StartGame ->
              ( { prevModel
                | model =
                  PlayerHealth.uniform Level.defaultSize Cell.default
                  |> Model.GeneratingLevel 0 Level.defaultSize
                }
              , Position 0 0
                |> Level.generator 0 Level.defaultSize
                |> Random.generate Message.GeneratedLevel
              )

            LevelSelect.Help ->
              ( { prevModel
                | model =
                  Model.HelpScreen HelpScreen.Instructions
                }
              , Cmd.none
              )
          
        LevelSelect.StartGameResult ->
          ( { prevModel
            | model =
              PlayerHealth.uniform Level.defaultSize Cell.default
              |> Model.GeneratingLevel 0 Level.defaultSize
            }
          , Position 0 0
            |> Level.generator 0 Level.defaultSize
            |> Random.generate Message.GeneratedLevel
          )
          

        LevelSelect.ViewHelpResult ->
          ( { prevModel
            | model =
              Model.HelpScreen HelpScreen.Instructions
            }
          , Cmd.none
          )
          

    ( Message.Prize prizeMessage, Model.Prize score levelBound prizeModel ) ->
      case
        Prize.update prizeMessage prizeModel
      of
        Prize.NoChange ->
          ( prevModel
          , Cmd.none
          )

        Prize.ChangeModel newPrizeModel ->
          ( { prevModel
            | model =
              Model.Prize score levelBound newPrizeModel
            }
            , Cmd.none
          )

        Prize.Finish { playerHealth } ->
          ( { prevModel
            | model =
              Model.GeneratingLevel (score + 1) levelBound playerHealth
            }
          , prizeModel.playerLocation
            |> Level.generator score prizeModel.levelBound
            |> Random.generate Message.GeneratedLevel
          )


    ( Message.Level levelMessage, Model.Level score levelBound levelModel ) ->
      case
        Level.update levelMessage (Box.fromPosition levelBound) levelModel
      of
        Level.NoChange cmd ->
          ( prevModel
          , Cmd.map Message.Level cmd
          )

        Level.InternalChange newLevelModel cmd ->
          ( { prevModel
            | model =
              Model.Level
                score
                levelBound
                newLevelModel
            }
          , Cmd.map Message.Level cmd
          )

        Level.Win newHealth damageAnimations ->
          ( { prevModel
            | model =
              Model.Prize
                score
                levelBound
                { playerHealth =
                  newHealth
                , damageAnimations =
                  damageAnimations
                , playerLocation =
                  levelModel.level.playerLocation
                , prizeLocation =
                  { x =
                    (levelBound.x ) // 2
                  , y =
                    (levelBound.y ) // 2
                  }
                , levelBound =
                  levelBound
                , cursorState =
                  Prize.Neutral
                , firstUpgrade =
                  Tuple.first levelModel.level.upgradePrizes
                , mode =
                  PrizeMode.Selecting
                , secondUpgrade =
                  Tuple.second levelModel.level.upgradePrizes
                }
            }
            , Cmd.none
          )

        Level.Exit ->
          ( { prevModel
            | model =
              Model.LevelSelect
                { selected =
                  LevelSelect.StartGame
                }
            }
          , Cmd.none
          )

        Level.Defeat newModel ->
          ( { prevModel
            | model =
              Model.Death score levelBound newModel
            }
          , Cmd.none
          )

    ( Message.GeneratedLevel level, Model.GeneratingLevel score levelBound playerHealth ) ->
      if
        PlayerHealth.alive playerHealth
      then
        ( { prevModel
          | model =
            level
              |> Level.init playerHealth
              |> Model.Level score levelBound
          }
        , Cmd.none
        )
      else
        ( { prevModel
          | model =
            level
              |> Level.init playerHealth
              |> Model.Death score levelBound
          }
        , Cmd.none
        )

    ( Message.Death deathMessage, Model.Death score levelBound levelModel) ->
      case
        Death.update deathMessage levelModel
      of
        Death.NoChange ->
          ( prevModel
          , Cmd.none
          )

        Death.InternalChange newLevelModel cmd ->
          ( { prevModel
            | model =
              Model.Death
                score
                levelBound
                newLevelModel
            }
          , Cmd.map Message.Level cmd
          )

        Death.Exit ->
          ( { prevModel
            | model =
              Model.LevelSelect LevelSelect.init
            }
          , Cmd.none
          )

    ( Message.Help helpMessage, Model.HelpScreen _ ) ->
      case
        helpMessage
      of
        HelpScreen.ToInstructions ->
          ( { prevModel
            | model =
              Model.HelpScreen HelpScreen.Instructions
            }
          , Cmd.none
          )
        HelpScreen.ToUpgrades ->
          ( { prevModel
            | model =
              Model.HelpScreen HelpScreen.Upgrades
            }
          , Cmd.none
          )
        HelpScreen.ToLevelSelect ->
          ( { prevModel
            | model =
              Model.LevelSelect { selected = LevelSelect.Help }
            }
          , Cmd.none
          )

    ( _, _ ) ->
      ( prevModel
      , Cmd.none
      )

