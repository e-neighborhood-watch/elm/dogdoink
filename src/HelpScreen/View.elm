module HelpScreen.View
  exposing
    ( view
    )


import Css
import Css.Global
  as Css
import Html.Styled
  as Html
  exposing
    ( Attribute
    , Html
    )
import Html.Styled.Attributes
  as HtmlAttr
import Html.Styled.Events
  as Html
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Events
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Font.Size
  as Font
import Data.Text.Type
  as Text
import Document.Type
  exposing
    ( Document
    )
import HelpScreen.Message.Type
  as Message
  exposing
    ( Message
    )
import HelpScreen.Model.Type
  exposing
    ( Model (..)
    )
import Settings.Color.Theme
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Size.Type
  exposing
    ( FontSize
    )
import Settings.Font.Type
  as Font
  exposing
    ( Font
    )
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )


toCss : Font -> FontSize -> Attribute msg
toCss font fontSize =
  HtmlAttr.css
    [ Css.fontSize (Css.pt (50 * Font.rawSize font fontSize))
    , Css.fontFamilies
      [ "Lucida Console"
      , "Courier"
      , "sans-serif"
      ]
    ]

paragraphs : Model -> Settings -> List (Html Message)
paragraphs model settings =
  case
    model
  of
    Instructions ->
      [ Html.p
        [
        ]
        [ Html.text "You are in a dungeon or something, with five drops of blood in each slot of your inventory. Just like in real life, you will die if you run out of blood."
        ]
      , Html.p
        [
        ]
        [ Html.text "However, you must spend one drop of blood from your current location to attack, and every step you take will cost one drop of blood as well. You will also die if you are cornered with no way to attack."
        ]
      , Html.p
        [
        ]
        [ Html.text "When monsters attack you, they will damage your inventory in multiple spaces, in a pattern corresponding to your shape."
        ]
      , Html.p
        [
        ]
        [ Html.text "Every time you complete a floor, you will receive a gift that you can place anywhere in your inventory. This may be more blood, or it may be something more unusual."
        ]
      , Html.p
        [ Html.onClick Message.ToUpgrades
        ]
        [ Html.text "NEXT PAGE"
        ]
      , Html.p
        [ Html.onClick Message.ToLevelSelect
        ]
        [ Html.text "BACK TO MAIN MENU"
        ]
      ]
    Upgrades ->
      [ Html.p
        [
        ]
        [ Html.text "Anything with a red border is considered vital, and will keep you alive."
        ]
      , Html.p
        [
        ]
        [ Html.text "Shields will protect your precious blood. If you are standing on a shield and are attacked, you lose one shield, but nothing else will be damaged. Additionally, shields are not reduced when you step on them. Shields are vital."
        ]
      , Html.p
        [
        ]
        [ Html.text "Mushrooms grow back over time. If you have a mushroom when you leave a room, in the next floor you will have one additional mushroom in that slot, up to a maximum of 9. Mushrooms are vital."
        ]
      , Html.p
        [
        ]
        [ Html.text "Swords let you attack with impunity. If you are standing on a sword, you do not have to spend anything to attack, and swords are not reduced when you step on them. However, swords are not vital, and do not stack."
        ]
      , Html.p
        [
        ]
        [ Html.text "Fangs let you drain blood from your enemies to replenish yourself. If you attack by spending a fang, the slot corresponding to the enemy will be refilled with five drops of blood. Fangs are not reduced when you step on them, but are not vital."
        ]
      , Html.p
        [ Html.onClick Message.ToInstructions
        ]
        [ Html.text "NEXT PAGE"
        ]
      , Html.p
        [ Html.onClick Message.ToLevelSelect
        ]
        [ Html.text "BACK TO MAIN MENU"
        ]
      ]
      

view : Model -> Settings -> Document Message
view model settings =
  let
    rectBox =
      { min =
        { x =
          -0.3
        , y =
          -0.3
        }
      , max =
        { x =
          7.3
        , y =
          7.3
        }
      }
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          8.2
        , y =
          8.2
        }
      }
  in
    { title =
      Language.fromText settings.language Text.SettingsTitle
    , viewBox =
      viewBox
    , body =
      [ Html.div
        [ HtmlAttr.align "center"
        , toCss Font.Title settings.fontSize 
        , HtmlAttr.css
          [ Css.color ( Css.hex (Theme.toHex settings.theme Color.Foreground) )
          , Css.width ( Css.pct 50 )
          , Css.margin Css.auto
          , Css.marginBottom ( Css.pt 10 )
          ]
        ]
        [ Html.text (Language.fromText settings.language Text.HelpTitle)
        ]
      , Html.div
        [ toCss Font.Text settings.fontSize 
        , HtmlAttr.css
          [ Css.color ( Css.hex (Theme.toHex settings.theme Color.Foreground) )
          , Css.width ( Css.pct 50 )
          , Css.margin Css.auto
          ]
        ]
        ( paragraphs model settings )
      ]
    }