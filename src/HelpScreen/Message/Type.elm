module HelpScreen.Message.Type
  exposing
    ( Message (..)
    )


type Message
  = ToLevelSelect
  | ToInstructions
  | ToUpgrades