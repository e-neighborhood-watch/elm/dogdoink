module Message.Type
  exposing
    ( Message (..)
    )


import Death.Message.Type
  as Death
import HelpScreen.Message.Type
  as HelpScreen
import Level.Message.Type
  as Level
import Level.Type
  exposing
    ( Level
    )
import LevelSelect.Message.Type
  as LevelSelect
import Prize.Message.Type
  as Prize
import Settings.Message.Type
  as Settings


type Message
  = Noop
  | OpenSettings
  | Level Level.Message
  | Prize Prize.Message
  | LevelSelect LevelSelect.Message
  | Settings Settings.Message
  | GeneratedLevel Level
  | Death Death.Message
  | Help HelpScreen.Message