module Prize.Model.Type
  exposing
    ( Model
    )


import Animated.Type
  exposing
    ( Animated
    )
import Level.Player.Health.Type
  as Player
import Level.Player.Health.Cell.Type
  as Health
import Prize.CursorState.Type
  as Prize
import Prize.Mode.Type
  as Prize
import Util.Position.Type
  exposing
    ( Position
    )


type alias Model =
  { playerHealth :
    Player.Health
  , damageAnimations :
    List (Animated (Position Int))
  , playerLocation :
    Position Int
  , levelBound :
    Position Int
  , cursorState :
    Prize.CursorState
  , firstUpgrade :
    Health.Cell
  , secondUpgrade :
    Health.Cell
  , prizeLocation :
    Position Int
  , mode :
    Prize.Mode
  }
