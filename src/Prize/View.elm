module Prize.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Events
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Text.Type
  as Text
import Document.Type
  exposing
    ( Document
    )
import Level.Player.Health.Cell.View
  as Cell
import Level.Player.Health.View
  as PlayerHealth
import Prize.CursorState.Type
  as CursorState
import Prize.Message.Type
  as Prize
import Prize.Message.Type
  as Message
import Prize.Mode.Type
  as Mode
import Prize.Model.Type
  as Prize
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )
import Util.Box.Type
  exposing
    ( Box
    )


view : Settings -> Box Int -> Prize.Model -> Document Prize.Message
view settings levelBox model =
  let
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          toFloat levelBox.max.x + 1.2
        , y =
          toFloat (levelBox.max.y * 2) + 2.5
        }
      }
    rectBox =
      { min =
        { x =
          0.0
        , y =
          0.0
        }
      , max =
        { x =
          toFloat levelBox.max.x
        , y =
          toFloat levelBox.max.y
        }
      }

    title =
      Language.fromText settings.language Text.PrizeTitle
  in
    { title =
      title
    , viewBox =
      viewBox
    , body =
      [ Svg.text_
        [ SvgAttr.css
          [ Css.property "dominant-baseline" "hanging"
          , Css.property "text-anchor" "middle"
          ]
        , SvgAttr.x (( (rectBox.min.x + rectBox.max.x) / 2 ) |> String.fromFloat)
        , SvgAttr.y "0"
        , Font.toCss Font.Title settings.fontSize
        , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
        ]
        [ Svg.text title
        ]
      , Cell.view settings { x = 1.3, y = 2.0 } model.firstUpgrade
        |> Svg.g
           [ Svg.onClick (Message.SelectCursorState CursorState.Left)
           ]
      , Cell.view settings { x = 3.3, y = 2.0 } model.secondUpgrade
        |> Svg.g
           [ Svg.onClick (Message.SelectCursorState CursorState.Right)
           ]
      , Svg.use
        [ SvgAttr.xlinkHref "#Character"
        , SvgAttr.x "2.075"
        , SvgAttr.y "1.85"
        , SvgAttr.fill (Theme.toHex settings.theme Color.Blood)
        , SvgAttr.transform
          ( case
              model.cursorState
            of
              CursorState.Left ->
                "rotate(90,2.575,2.25)"
              CursorState.Right ->
                "rotate(270,2.575,2.25)"
              CursorState.Neutral ->
                "rotate(0,2.575,2.25)"
          )
        ]
        [
        ]
      , PlayerHealth.view settings model.damageAnimations Nothing levelBox.max.y model.playerHealth
      ]
      |> (\ x y -> y ++ x)
        ( case
            model.mode
          of
            Mode.Selecting ->
              [
              ]
            Mode.Placing ->
              ( case
                  model.cursorState
                of
                  CursorState.Neutral ->
                    []
                  _ ->
                    [ Svg.rect
                      [ SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
                      , SvgAttr.strokeWidth "0.05"
                      , SvgAttr.fill "none"
                      , SvgAttr.width "1.1"
                      , SvgAttr.height "1.1"
                      , SvgAttr.rx "0.2"
                      , SvgAttr.y "1.75"
                      , SvgAttr.x
                        ( case
                            model.cursorState
                          of
                            CursorState.Left ->
                              "1.03"
                            _ ->
                              "3.03"
                        )
                      ]
                      [
                      ]
                    , Svg.rect
                      [ SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
                      , SvgAttr.strokeWidth "0.05"
                      , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
                      , SvgAttr.width "1.1"
                      , SvgAttr.height "1.1"
                      , SvgAttr.rx "0.2"
                      , SvgAttr.x
                        ( model.prizeLocation.x
                        |> toFloat
                        |> (+) 0.04
                        |> String.fromFloat
                        )
                      , SvgAttr.y
                        ( model.prizeLocation.y
                        |> (+) (levelBox.max.y + 1)
                        |> toFloat
                        |> (+) 0.04
                        |> String.fromFloat
                        )
                      ]
                      [
                      ]
                    , Cell.view
                      settings
                      { x =
                        model.prizeLocation.x
                        |> toFloat
                        |> (+) 0.3
                      , y =
                        model.prizeLocation.y
                        |> (+) (levelBox.max.y + 1)
                        |> toFloat
                        |> (+) 0.3
                      }
                      ( case
                          model.cursorState
                        of
                          CursorState.Left ->
                            model.firstUpgrade
                          _ ->
                            model.secondUpgrade
                      )
                      |> Svg.g []
                    ]
              )
        )
    }
