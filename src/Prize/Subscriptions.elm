module Prize.Subscriptions
  exposing
    ( subscriptions
    )


import Browser.Events
  as Browser
import Dict


import Keys
import Prize.CursorState.Type
  as CursorState
import Prize.Message.Type
  as Message
  exposing
    ( Message
    )
import Prize.Mode.Type
  as Mode
import Prize.Model.Type
  exposing
    ( Model
    )
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction


subscriptions : Settings -> Model -> Sub Message
subscriptions settings { mode, cursorState } =
  case
    mode
  of
    Mode.Selecting ->
      [ Dict.singleton settings.controls.prizeSelectLeft (Message.SetCursorState CursorState.Left)
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      , Dict.singleton settings.controls.prizeSelectRight (Message.SetCursorState CursorState.Right)
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      ]
      |> (++)
        ( if
            cursorState /= CursorState.Neutral
          then
            [ Dict.singleton settings.controls.levelSelectSelect (Message.SetMode Mode.Placing)
              |> Keys.makeKeyDecoder
              |> Browser.onKeyDown
            ]
          else
            [
            ]
        )
      |> Sub.batch
    Mode.Placing ->
      [ Dict.singleton settings.controls.playerMoveNorth (Message.MoveUpgrade Direction.North)
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      , Dict.singleton settings.controls.playerMoveSouth (Message.MoveUpgrade Direction.South)
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      , Dict.singleton settings.controls.playerMoveEast (Message.MoveUpgrade Direction.East)
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      , Dict.singleton settings.controls.playerMoveWest (Message.MoveUpgrade Direction.West)
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      , Dict.singleton settings.controls.levelSelectSelect Message.SetUpgrade
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      , Dict.singleton settings.controls.backUp (Message.SetMode Mode.Selecting)
        |> Keys.makeKeyDecoder
        |> Browser.onKeyDown
      ]
      |> Sub.batch
