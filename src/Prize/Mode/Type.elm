module Prize.Mode.Type
  exposing
    ( Mode (..)
    )


type Mode
  = Selecting
  | Placing
