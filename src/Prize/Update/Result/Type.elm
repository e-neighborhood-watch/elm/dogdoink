module Prize.Update.Result.Type
  exposing
    ( Result (..)
    )


import Prize.Model.Type
  as Prize


type Result
  = NoChange
  | ChangeModel Prize.Model
  | Finish Prize.Model
