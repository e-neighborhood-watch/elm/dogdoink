module Prize.CursorState.Type
  exposing
    ( CursorState (..)
    )


type CursorState
  = Left
  | Right
  | Neutral
