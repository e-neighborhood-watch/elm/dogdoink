module Prize.Message.Type
  exposing
    ( Message (..)
    )


import Prize.CursorState.Type
  exposing
    ( CursorState
    )
import Prize.Mode.Type
  as Prize
import Util.Direction.Type
  exposing
    ( Direction
    )

type Message
  = SetMode Prize.Mode
  | SetCursorState CursorState
  | SelectCursorState CursorState
  | MoveUpgrade Direction
  | SetUpgrade
