module Prize.Update
  exposing
    ( update
    )


import Level.Player.Health.Set
  as PlayerHealth
import Prize.CursorState.Type
  as CursorState
import Prize.Message.Type
  as Message
  exposing
    ( Message
    )
import Prize.Mode.Type
  as Mode
import Prize.Model.Type
  exposing
    ( Model
    )
import Prize.Update.Result.Type
  as Update
import Util.Box.From
  as Box
import Util.Box.Snap
  as Box
import Util.Direction.ToOffset
  as Direction
import Util.Position.Move
  as Position


update : Message -> Model -> Update.Result
update msg model =
  case
    msg
  of
    Message.SetMode newMode ->
      Update.ChangeModel
        { model
        | mode =
          newMode
        }

    Message.SetCursorState newCursorState ->
      Update.ChangeModel
        { model
        | cursorState =
          newCursorState
        }

    Message.SelectCursorState newCursorState ->
      case
        newCursorState
      of
        CursorState.Neutral ->
          Update.NoChange
        _ ->
          Update.ChangeModel
            { model
            | mode =
              Mode.Placing
            , cursorState =
              newCursorState
            }

    Message.MoveUpgrade dir ->
      Update.ChangeModel
        { model
        | prizeLocation =
          model.prizeLocation
            |> Position.move (Direction.toOffset dir)
            |> Box.snap (Box.fromPosition model.levelBound)
        }

    Message.SetUpgrade ->
      Update.Finish
        { model
        | playerHealth =
          case
            model.cursorState
          of
            CursorState.Left ->
              PlayerHealth.set model.prizeLocation model.firstUpgrade model.playerHealth
            CursorState.Right ->
              PlayerHealth.set model.prizeLocation model.secondUpgrade model.playerHealth
            CursorState.Neutral ->
              model.playerHealth
        }
