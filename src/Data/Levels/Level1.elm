module Data.Levels.Level1
  exposing
    ( level
    )


import Dict
  exposing
    ( Dict
    )


import Data.Text.Type
  as Text
import Level.Type
  exposing
    ( Level
    )


level : Level
level =
  { monsterMap =
    Dict.singleton
      (2, 4)
      {}
  , playerLocation =
    { x =
      0
    , y =
      0
    }
  , exitLocation =
    { x =
      3
    , y =
      4
    }
  }
