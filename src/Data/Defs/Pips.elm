module Data.Defs.Pips
  exposing
    ( defs
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


pipDef : String -> Svg a
pipDef name =
  Svg.use
    [ SvgAttr.xlinkHref ("Data/Assets/" ++ name ++ ".svg#content")
    , SvgAttr.id name
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]


defs : List (Svg a)
defs =
  pipDef "BadPips" ::
    List.map
      ( \ n ->
        pipDef ("Pips" ++ String.fromInt n)
      )
      [ 0
      , 1
      , 2
      , 3
      , 4
      , 5
      , 6
      ]
