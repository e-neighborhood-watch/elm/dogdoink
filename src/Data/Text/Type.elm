module Data.Text.Type
  exposing
    ( Text (..)
    )


type Text
  = HelloWorld
  | LevelSelectTitle
  | SettingsTitle
  | LevelTitle
  | LevelSelectStartGame
  | LevelSelectHelp
  | GeneratingLevelTitle
  | DeathTitle
  | DeathSubtitle
  | PrizeTitle
  | HelpTitle
  | Score
