module Data.Defs
  exposing
    ( defs
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Defs.Pips
  as Pips

defs : Svg a
defs =
  [ Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Drop.svg#content"
    , SvgAttr.id "Drop"
    , SvgAttr.width "3"
    , SvgAttr.height "3"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Fang.svg#content"
    , SvgAttr.id "Fang"
    , SvgAttr.width "3"
    , SvgAttr.height "3"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Mushroom.svg#content"
    , SvgAttr.id "Mushroom"
    , SvgAttr.width "3"
    , SvgAttr.height "3"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/SettingsWheel.svg#content"
    , SvgAttr.id "SettingsWheel"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/FontLarge.svg#content"
    , SvgAttr.id "FontLarge"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/FontMedium.svg#content"
    , SvgAttr.id "FontMedium"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/FontSmall.svg#content"
    , SvgAttr.id "FontSmall"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/DarkTheme.svg#content"
    , SvgAttr.id "DarkTheme"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/LightTheme.svg#content"
    , SvgAttr.id "LightTheme"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/DarkAltTheme.svg#content"
    , SvgAttr.id "DarkAltTheme"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Gridlines.svg#content"
    , SvgAttr.id "Gridlines"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/CancelIcon.svg#content"
    , SvgAttr.id "CancelIcon"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/AcceptIcon.svg#content"
    , SvgAttr.id "AcceptIcon"
    , SvgAttr.width "0.6"
    , SvgAttr.height "0.6"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Character.svg#content"
    , SvgAttr.id "Character"
    , SvgAttr.width "1"
    , SvgAttr.height "1"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Exit.svg#content"
    , SvgAttr.id "Exit"
    , SvgAttr.width "1"
    , SvgAttr.height "1"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Die.svg#content"
    , SvgAttr.id "Die"
    , SvgAttr.x "-0.24"
    , SvgAttr.y "-0.24"
    , SvgAttr.width "1.1"
    , SvgAttr.height "1.1"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Sword.svg#content"
    , SvgAttr.id "Sword"
    , SvgAttr.x "0"
    , SvgAttr.y "0"
    , SvgAttr.width ".7"
    , SvgAttr.height ".7"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Shield.svg#content"
    , SvgAttr.id "Shield"
    , SvgAttr.width "3"
    , SvgAttr.height "3"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Drop2.svg#content"
    , SvgAttr.id "DropCentered"
    , SvgAttr.x "-0.0625"
    , SvgAttr.y "-0.03125"
    , SvgAttr.width ".7"
    , SvgAttr.height ".7"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/FencePost.svg#content"
    , SvgAttr.id "FencePost"
    , SvgAttr.x "-0.14"
    , SvgAttr.y "-0.14"
    , SvgAttr.width "0.3"
    , SvgAttr.height "0.3"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Monsters/Plus.svg#content"
    , SvgAttr.id "PlusMonster"
    , SvgAttr.x "0.0625"
    , SvgAttr.y "0.0625"
    , SvgAttr.width "1"
    , SvgAttr.height "1"
    ]
    [
    ]
  , Svg.use
    [ SvgAttr.xlinkHref "Data/Assets/Monsters/Circle.svg#content"
    , SvgAttr.id "CircleMonster"
    , SvgAttr.x "0.0625"
    , SvgAttr.y "0.0625"
    , SvgAttr.width "1"
    , SvgAttr.height "1"
    ]
    [
    ]
  ]
    |> (++) Pips.defs
    |> Svg.defs []
