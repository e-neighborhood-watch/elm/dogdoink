module Data.Font.Size
  exposing
    ( rawSize
    )


import Settings.Font.Size.Type
  as FontSize
  exposing
    ( FontSize
    )
import Settings.Font.Type
  as Font
  exposing
    ( Font
    )


rawSize : Font -> FontSize -> Float
rawSize font fontSize =
  case
    font
  of
    Font.Text ->
      case
        fontSize
      of
        FontSize.Small ->
          0.2
        FontSize.Medium ->
          0.3
        FontSize.Large ->
          0.5

    Font.Title ->
      case
        fontSize
      of
        FontSize.Small ->
          0.2
        FontSize.Medium ->
          0.6
        FontSize.Large ->
          1.0
