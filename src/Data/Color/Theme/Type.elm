module Data.Color.Theme.Type
  exposing
    ( ThemeColor (..)
    )


type ThemeColor
  = Background
  | Accent
  | Monster
  | Foreground
  | Damage
  | Blood
  | Growth