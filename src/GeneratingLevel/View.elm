module GeneratingLevel.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Text.Type
  as Text
import Document.Type
  exposing
    ( Document
    )
import Settings.Color.Theme
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )


view : Settings -> Document a
view settings =
  let
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          8.2
        , y =
          8.2
        }
      }
  in
    { title =
      Language.fromText settings.language Text.GeneratingLevelTitle
    , viewBox =
      viewBox
    , body =
      [ Svg.text_
        [ SvgAttr.css
          [ Css.property "dominant-baseline" "central"
          , Css.property "text-anchor" "middle"
          ]
        , Font.toCss Font.Title settings.fontSize
        , Color.Foreground
          |> Theme.toHex settings.theme
          |> SvgAttr.fill 
        , (viewBox.min.x + viewBox.max.x) / 2
          |> String.fromFloat
          |> SvgAttr.x
        , (viewBox.min.y + viewBox.max.y) / 2
          |> String.fromFloat
          |> SvgAttr.y
        ]
        [ Language.fromText settings.language Text.GeneratingLevelTitle
          |> Svg.text
        ]
      ]
    }
