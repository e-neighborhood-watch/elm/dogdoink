module Util.Direction.FromOffset
  exposing
    ( fromOffset
    )


import Util.Direction.Type
  exposing
    ( Direction (..)
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


fromOffset : Offset Int -> Maybe Direction
fromOffset { dx, dy } =
  if
    dx == 1
      && dy == 0
  then
    Just East
  else
    if
      dx == -1
        && dy == 0
    then
      Just West
    else
      if
        dx == 0
          && dy == 1
      then
        Just South
      else
        if
          dx == 0
            && dy == -1
        then
          Just North
        else
          Nothing
