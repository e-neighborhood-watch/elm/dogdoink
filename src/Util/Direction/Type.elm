module Util.Direction.Type
  exposing
    ( Direction (..)
    )


type Direction
  = North
  | South
  | East
  | West
