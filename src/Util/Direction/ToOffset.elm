module Util.Direction.ToOffset
  exposing
    ( toOffset
    )


import Util.Direction.Type
  exposing
    ( Direction (..)
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


toOffset : Direction -> Offset Int
toOffset dir =
  case
    dir
  of
    North ->
      { dx =
        0
      , dy =
        -1
      }

    South ->
      { dx =
        0
      , dy =
        1
      }

    East ->
      { dx =
        1
      , dy =
        0
      }

    West ->
      { dx =
        -1
      , dy =
        0
      }
