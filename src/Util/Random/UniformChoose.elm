module Util.Random.UniformChoose
  exposing
    ( uniformChoose
    )


import Random


uniformChooseHelper : Int -> List a -> a -> List a -> Random.Generator (a, List a)
uniformChooseHelper prevSteps tossed held rest =
  case
    rest
  of
    [] ->
      Random.constant
        ( held
        , tossed
        )

    x :: xs ->
      Random.int 0 prevSteps
        |> Random.andThen
          ( \ n ->
            if
              n == 0
            then
              uniformChooseHelper
                (prevSteps + 1)
                (held :: tossed)
                x
                xs
            else
              uniformChooseHelper
                (prevSteps + 1)
                (x :: tossed)
                held
                xs
          )


uniformChoose : a -> List a -> Random.Generator (a, List a)
uniformChoose =
  uniformChooseHelper 1 []

