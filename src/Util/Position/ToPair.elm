module Util.Position.ToPair
  exposing
    ( toPair
    )


import Util.Position.Type
  exposing
    ( Position
    )


toPair : Position a -> (a, a)
toPair { x, y } =
  (x, y)