module Util.Position.Type
  exposing
    ( Position
    )


type alias Position a =
  { x :
    a
  , y :
    a
  }
