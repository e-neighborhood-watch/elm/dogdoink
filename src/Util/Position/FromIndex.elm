module Util.Position.FromIndex
  exposing
    ( fromIndex
    )


import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.Type
  exposing
    ( Position
    )


fromIndex : Box Int -> Int -> Position Int
fromIndex levelBox index =
  let
    width =
      levelBox.max.x - levelBox.min.x
  in
    { x =
      modBy width index + levelBox.min.x
    , y =
      index // width + levelBox.min.y
    }
