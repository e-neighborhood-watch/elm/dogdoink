module Util.Position.Move
  exposing
    ( move
    )

import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Type
  exposing
    ( Position
    )


move : Offset number -> Position number -> Position number
move { dx, dy } { x, y } =
  { x =
    x + dx
  , y =
    y + dy
  }
