module Util.Position.FromPair
  exposing
    ( fromPair
    )


import Util.Position.Type
  exposing
    ( Position
    )


fromPair : (a, a) -> Position a
fromPair (x, y) =
  { x
  = x
  , y
  = y
  }