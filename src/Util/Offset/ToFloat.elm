module Util.Offset.ToFloat
  exposing
    ( toFloat
    )


import Util.Offset.Type
  exposing
    ( Offset
    )


toFloat : Offset Int -> Offset Float
toFloat { dx, dy } =
  { dx =
    Basics.toFloat dx
  , dy =
    Basics.toFloat dy
  }
