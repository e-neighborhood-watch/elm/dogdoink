module Util.Offset.Scale
  exposing
    ( scale
    )


import Util.Offset.Type
  exposing
    ( Offset
    )


scale : number -> Offset number -> Offset number
scale a { dx, dy } =
  { dx =
    a * dx
  , dy =
    a * dy
  }
