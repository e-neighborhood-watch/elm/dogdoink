module Util.Offset.Type
  exposing
    ( Offset
    )


type alias Offset a =
  { dx :
    a
  , dy :
    a
  }
