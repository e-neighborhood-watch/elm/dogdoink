module Util.Box.Type
  exposing
    ( Box
    )


import Util.Position.Type
  exposing
    ( Position
    )


type alias Box a =
  { min :
    Position a
  , max :
    Position a
  }
