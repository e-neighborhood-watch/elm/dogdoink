module Util.Box.From
  exposing
    ( fromPosition
    )


import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.Type
  exposing
    ( Position
    )


fromPosition : Position number -> Box number
fromPosition bound =
  { min =
    { x =
      0
    , y =
      0
    }
  , max =
    bound
  }
