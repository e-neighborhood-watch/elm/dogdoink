module Util.Box.Snap
  exposing
    ( snap
    )


import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.Type
  exposing
    ( Position
    )


snap : Box number -> Position number -> Position number
snap box { x, y } =
  { x =
    x
    |> max box.min.x
    |> min (box.max.x - 1)
  , y =
    y
    |> max box.min.y
    |> min (box.max.y - 1)
  }
