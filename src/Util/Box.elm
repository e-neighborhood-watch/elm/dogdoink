module Util.Box
  exposing
    ( within
    )


import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.Type
  exposing
    ( Position
    )


within : Box comparable -> Position comparable -> Bool
within { min, max } { x, y } =
  (  x >= min.x
  && y >= min.y
  && x < max.x
  && y < max.y
  )
