module Settings.Font.Css
  exposing
    ( toCss
    )


import Css
import Svg.Styled
  exposing
    ( Attribute
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Font.Size
  as Font
import Settings.Font.Size.Type
  exposing
    ( FontSize
    )
import Settings.Font.Type
  exposing
    ( Font
    )


toCss : Font -> FontSize -> Attribute msg
toCss font fontSize =
  SvgAttr.css
    [ Css.fontSize (Css.px (Font.rawSize font fontSize))
    , Css.fontFamilies
      [ "Lucida Console"
      , "Courier"
      , "sans-serif"
      ]
    ]
