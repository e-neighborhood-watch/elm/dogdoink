module Settings.Font.Size.Type
  exposing
    ( FontSize (..)
    )


type FontSize
  = Small
  | Medium
  | Large
