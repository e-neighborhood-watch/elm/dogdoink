module Settings.Font.Type
  exposing
    ( Font (..)
    )


type Font
  = Title
  | Text
