module Settings.Color.Theme.DarkAlt
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )


toHex : ThemeColor -> String
toHex color =
  case
    color
  of
    Color.Background ->
      "#242424"
    Color.Accent ->
      "#575757"
    Color.Monster ->
      "#BA2424"
    Color.Foreground ->
      "#F34242"
    Color.Blood ->
      "#F34242"
    Color.Damage ->
      "#F34242"
    Color.Growth ->
      "#F34242"