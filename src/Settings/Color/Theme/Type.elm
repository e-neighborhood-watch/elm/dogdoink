module Settings.Color.Theme.Type
  exposing
    ( Theme (..)
    )


type Theme
  = Dark
  | Light
  | DarkAlt
