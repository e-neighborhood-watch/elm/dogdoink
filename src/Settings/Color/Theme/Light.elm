module Settings.Color.Theme.Light
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )


toHex : ThemeColor -> String
toHex color =
  case
    color
  of
    Color.Background ->
      "#CACACA"
    Color.Accent ->
      "#5A5A5A"
    Color.Monster ->
      "#A43A3A"
    Color.Foreground ->
      "#343434"
    Color.Blood ->
      "#AF1010"
    Color.Damage ->
      "#AF1010"
    Color.Growth ->
      "#008A0B"
