module Settings.Color.Theme.Init
  exposing
    ( init
    )


import Settings.Color.Theme.Type
  as Theme
  exposing
    ( Theme
    )


init : Theme
init =
  Theme.Dark
