module Settings.Color.Theme.Dark
  exposing
    ( toHex
    )


import Data.Color.Theme.Type
  as Color
  exposing
    ( ThemeColor
    )


toHex : ThemeColor -> String
toHex color =
  case
    color
  of
    Color.Background ->
      "#242424"
    Color.Accent ->
      "#575757"
    Color.Monster ->
      "#BA2424"
    Color.Foreground ->
      "#007f7f"
    Color.Blood ->
      "#F34242"
    Color.Damage ->
      "#F3F342"
    Color.Growth ->
      "#00A819"
