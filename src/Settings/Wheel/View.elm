module Settings.Wheel.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Events
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Message.Type
  as Game
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
  exposing
    ( Theme
    )
import Util.Box.Type
  exposing
    ( Box
    )


view : Box Float -> Theme -> Svg Game.Message
view { min } theme =
  Svg.use
    [ SvgAttr.xlinkHref "#SettingsWheel"
    , SvgAttr.x (min.x - 0.5 |> String.fromFloat)
    , SvgAttr.y (min.y + 0.5 |> String.fromFloat)
    , SvgAttr.fill (Theme.toHex theme Color.Foreground)
    , Svg.onClick Game.OpenSettings
    ]
    [
    ]
