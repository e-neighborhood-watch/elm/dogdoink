module Settings.View
  exposing
    ( view
    )

import Css
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Events
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Text.Type
  as Text
import Document.Type
  exposing
    ( Document
    )
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
  exposing
    ( Theme
    )
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Font.Size.Type
  as FontSize
import Settings.Language
  as Language
import Settings.Message.Type
  as Message
  exposing
    ( Message
    )
import Settings.Model.Type
  exposing
    ( Model
    )


settingsButton : Bool -> Theme -> Float -> Float -> String -> Message -> Svg Message
settingsButton on theme x y iconName message =
  Svg.g
    [ Svg.onClick message
    ]
    [ Svg.rect
      [ SvgAttr.x (x |> String.fromFloat)
      , SvgAttr.y (y |> String.fromFloat)
      , SvgAttr.rx (0.1 |> String.fromFloat)
      , SvgAttr.width "0.5"
      , SvgAttr.height "0.5"
      , SvgAttr.fill (Theme.toHex theme Color.Background)
      , SvgAttr.stroke (Theme.toHex theme Color.Background)
      , SvgAttr.strokeWidth "0.2"
      ]
      [
      ]
    , Svg.rect
      [ SvgAttr.x (x |> String.fromFloat)
      , SvgAttr.y (y |> String.fromFloat)
      , SvgAttr.rx (0.1 |> String.fromFloat)
      , SvgAttr.width "0.5"
      , SvgAttr.height "0.5"
      , SvgAttr.fill (Theme.toHex theme Color.Background)
      , SvgAttr.stroke (Theme.toHex theme Color.Foreground)
      , SvgAttr.strokeWidth "0.05"
      ]
      [
      ]
    , Svg.use
      [ SvgAttr.xlinkHref iconName
      , SvgAttr.x (String.fromFloat (x - 0.05))
      , SvgAttr.y (String.fromFloat (y - 0.05))
      , SvgAttr.fill (Theme.toHex theme Color.Foreground)
      , SvgAttr.stroke (Theme.toHex theme Color.Accent)
      , SvgAttr.strokeWidth "0.4"
      ]
      [
      ]
    , if
        on
      then
        Svg.rect
          [ SvgAttr.x (x |> (+) -0.1 |> String.fromFloat)
          , SvgAttr.y (y |> (+) -0.1 |> String.fromFloat)
          , SvgAttr.rx (0.2 |> String.fromFloat)
          , SvgAttr.width "0.7"
          , SvgAttr.height "0.7"
          , SvgAttr.fill "none"
          , SvgAttr.stroke (Theme.toHex theme Color.Foreground)
          , SvgAttr.strokeWidth "0.05"
          ]
          [
          ]
      else
        Svg.g [] []
    ]


view : Model -> Document Message
view model =
  let
    rectBox =
      { min =
        { x =
          -0.3
        , y =
          -0.3
        }
      , max =
        { x =
          7.3
        , y =
          7.3
        }
      }
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          8.2
        , y =
          8.2
        }
      }
  in
    { title =
      Language.fromText model.settings.language Text.SettingsTitle
    , viewBox =
      viewBox
    , body =
      [ Svg.rect
        [ SvgAttr.x (rectBox.min.x |> String.fromFloat)
        , SvgAttr.y (rectBox.min.y |> String.fromFloat)
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Background)
        , SvgAttr.width (rectBox.max.x - rectBox.min.x |> String.fromFloat)
        , SvgAttr.height (rectBox.max.y - rectBox.min.y |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex model.settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        ]
        [
        ]
      , Svg.text_
        [ SvgAttr.css
          [ Css.property "dominant-baseline" "hanging"
          , Css.property "text-anchor" "middle"
          ]
        , SvgAttr.x (( (rectBox.min.x + rectBox.max.x) / 2 ) |> String.fromFloat)
        , SvgAttr.y "0"
        , Font.toCss Font.Title model.settings.fontSize
        , SvgAttr.fill (Theme.toHex model.settings.theme Color.Foreground)
        ]
        [ Svg.text "Settings"
        ]
      , settingsButton
        ( model.settings.fontSize == FontSize.Small )
        model.settings.theme
        2.2
        1.2
        "#FontSmall"
        ( Message.SetFontSize FontSize.Small )
      , settingsButton
        ( model.settings.fontSize == FontSize.Medium )
        model.settings.theme
        3.2
        1.2
        "#FontMedium"
        ( Message.SetFontSize FontSize.Medium )
      , settingsButton
        ( model.settings.fontSize == FontSize.Large )
        model.settings.theme
        4.2
        1.2
        "#FontLarge"
        ( Message.SetFontSize FontSize.Large )
      , settingsButton
        ( model.settings.theme == Theme.Light )
        Theme.Light
        2.2
        2.2
        "#LightTheme"
        ( Message.SetTheme Theme.Light )
      , settingsButton
        ( model.settings.theme == Theme.Dark )
        Theme.Dark
        3.2
        2.2
        "#DarkTheme"
        ( Message.SetTheme Theme.Dark )
      , settingsButton
        ( model.settings.theme == Theme.DarkAlt )
        Theme.DarkAlt
        4.2
        2.2
        "#DarkAltTheme"
        ( Message.SetTheme Theme.DarkAlt )
      , settingsButton
        model.settings.drawGridlines
        model.settings.theme
        3.2
        3.2
        "#Gridlines"
        Message.ToggleGridlines
      , Svg.use
        [ SvgAttr.xlinkHref "#AcceptIcon"
        , SvgAttr.x (rectBox.max.x - 1.8 |> String.fromFloat)
        , SvgAttr.y (rectBox.max.y - 0.8 |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick Message.SaveAndExit
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#CancelIcon"
        , SvgAttr.x (rectBox.max.x - 0.8 |> String.fromFloat)
        , SvgAttr.y (rectBox.max.y - 0.8 |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex model.settings.theme Color.Foreground)
        , Svg.onClick Message.Cancel
        ]
        [
        ]
      ]
    }
