module Settings.Init
  exposing
    ( init
    )


import Settings.Color.Theme.Init
  as Theme
import Settings.Controls.Init
  as Controls
import Settings.Font.Size.Init
  as FontSize
import Settings.Language.Type
  as Language
import Settings.Type
  exposing
    ( Settings
    )


init : Settings
init =
  { controls =
    Controls.init
  , language =
    Language.EN
  , theme =
    Theme.init
  , fontSize =
    FontSize.init
  , drawGridlines =
    True
  }
