module Settings.Update
  exposing
    ( update
    )


import Settings.Message.Type
  as Message
  exposing
    ( Message
    )
import Settings.Model.Type
  exposing
    ( Model
    )
import Settings.Update.Result.Type
  as Update

update : Message -> Model -> Update.Result
update msg { settings } =
  case
    msg
  of
    Message.Cancel ->
      Update.Cancel

    Message.SaveAndExit ->
      Update.SaveAndExit

    Message.ToggleGridlines ->
      Update.ChangeModel
        { settings =
          { settings
          | drawGridlines =
            not settings.drawGridlines
          }
        }

    Message.SetFontSize newSize ->
      Update.ChangeModel
        { settings =
          { settings
          | fontSize =
            newSize
          }
        }

    Message.SetTheme newTheme ->
      Update.ChangeModel
        { settings =
          { settings
          | theme =
            newTheme
          }
        }

    Message.SetLanguage newLang ->
      Update.ChangeModel
        { settings =
          { settings
          | language =
            newLang
          }
        }
