module Settings.Language.EN
  exposing
    ( fromText
    )


import Data.Text.Type
  as Text
  exposing
    ( Text
    )


fromText : Text -> String
fromText text =
  case
    text
  of
    Text.HelloWorld ->
      "Hello, World!"
    Text.LevelSelectTitle ->
      "Level select"
    Text.LevelSelectStartGame ->
      "Start Game"
    Text.LevelSelectHelp ->
      "Help"
    Text.SettingsTitle ->
      "Settings"
    Text.LevelTitle ->
      "Game"
    Text.GeneratingLevelTitle ->
      "Generating level..."
    Text.DeathTitle ->
      "You have died."
    Text.DeathSubtitle ->
      "Press enter to restart..."
    Text.PrizeTitle ->
      "Choose a gift..."
    Text.HelpTitle ->
      "How to play"
    Text.Score ->
      "Floor: "
