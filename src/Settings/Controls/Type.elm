module Settings.Controls.Type
  exposing
    ( Controls
    )

type alias Controls =
  { levelSelectPrevious :
    String
  , levelSelectNext :
    String
  , levelSelectSelect :
    String
  , playerMoveNorth :
    String
  , playerMoveSouth :
    String
  , playerMoveEast :
    String
  , playerMoveWest :
    String
  , prizeSelectLeft :
    String
  , prizeSelectRight :
    String
  , backUp :
    String
  }
