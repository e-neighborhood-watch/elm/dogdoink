module Settings.Controls.Init
  exposing
    ( init
    )


import Settings.Controls.Type
  exposing
    ( Controls
    )


init : Controls
init =
  { levelSelectPrevious =
    "ArrowUp"
  , levelSelectNext =
    "ArrowDown"
  , levelSelectSelect =
    "Enter"
  , playerMoveNorth =
    "ArrowUp"
  , playerMoveSouth =
    "ArrowDown"
  , playerMoveEast =
    "ArrowRight"
  , playerMoveWest =
    "ArrowLeft"
  , prizeSelectLeft =
    "ArrowLeft"
  , prizeSelectRight =
    "ArrowRight"
  , backUp =
    "Escape"
  }
