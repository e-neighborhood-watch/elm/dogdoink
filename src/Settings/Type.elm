module Settings.Type
  exposing
    ( Settings
    , WithSettings
    )


import Settings.Color.Theme.Type
  exposing
    ( Theme
    )
import Settings.Controls.Type
  exposing
    ( Controls
    )
import Settings.Font.Size.Type
  exposing
    ( FontSize
    )
import Settings.Language.Type
  exposing
    ( Language
    )


type alias Settings =
  { controls :
    Controls
  , language :
    Language
  , theme :
    Theme
  , fontSize :
    FontSize
  , drawGridlines :
    Bool
  }


type alias WithSettings m =
  { m
  | settings :
    Settings
  }
