module Settings.Message.Type
  exposing
    ( Message (..)
    )


import Settings.Color.Theme.Type
  exposing
    ( Theme
    )
import Settings.Font.Size.Type
  exposing
    ( FontSize
    )
import Settings.Language.Type
  exposing
    ( Language
    )


type Message
  = SetLanguage Language
  | SetTheme Theme
  | SetFontSize FontSize
  | ToggleGridlines
  | Cancel
  | SaveAndExit
