module Level.Walls.View
  exposing
    ( view
    )


import Set
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Walls.Type
  exposing
    ( Walls
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Box.Type
  exposing
    ( Box
    )


view : Settings -> Box Int -> Walls -> Svg a
view settings levelBox { vertical, horizontal } =
  let
    horizontalWalls : List (Svg a)
    horizontalWalls =
      List.concatMap
        ( \ (x, y) ->
          [ Svg.line
            [ SvgAttr.x1 (x |> toFloat |> String.fromFloat)
            , SvgAttr.y1 (y |> toFloat |> String.fromFloat)
            , SvgAttr.x2 (x + 1 |> toFloat |> String.fromFloat)
            , SvgAttr.y2 (y |> toFloat |>  String.fromFloat)
            , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
            , SvgAttr.strokeWidth "0.1"
            ]
            [
            ]
          ]
        )
        (Set.toList horizontal)

    verticalWalls : List (Svg a)
    verticalWalls =
      List.concatMap
        ( \ (x, y) ->
          [ Svg.line
            [ SvgAttr.x1 (x |> toFloat |> String.fromFloat)
            , SvgAttr.y1 (y |> toFloat |> String.fromFloat)
            , SvgAttr.x2 (x |> toFloat |> String.fromFloat)
            , SvgAttr.y2 (y + 1 |> toFloat |> String.fromFloat)
            , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
            , SvgAttr.strokeWidth "0.1"
            ]
            [
            ]
          ]
        )
        (Set.toList vertical)

    fencePosts : List (Svg a)
    fencePosts =
      List.concatMap
        ( \ x ->
          List.concatMap
            ( \ y ->
              if
                Set.member (x, y) vertical
                || Set.member (x, y) horizontal
                || Set.member (x-1, y) horizontal
                || Set.member (x, y-1) vertical
              then
                [ Svg.use
                  [ SvgAttr.xlinkHref "#FencePost"
                  , SvgAttr.x (x |> String.fromInt)
                  , SvgAttr.y (y |> String.fromInt)
                  , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
                  ]
                  [
                  ]
                ]
              else
                [
                ]
            )
            ( List.range levelBox.min.y levelBox.max.y )
        )
        ( List.range levelBox.min.x levelBox.max.x )
  in
    Svg.g [] (horizontalWalls ++ verticalWalls ++ fencePosts)

