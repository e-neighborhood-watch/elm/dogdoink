module Level.Walls.Type
  exposing
    ( Walls
    )


import Set
  exposing
    ( Set
    )


type alias Walls =
  { horizontal :
    Set (Int, Int)
  , vertical :
    Set (Int, Int)
  }
