module Level.Walls.Generator
  exposing
    ( generator
    )


import Random
import Set
  exposing
    ( Set
    )


import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  as Direction
  exposing
    ( Direction
    )
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )
import Util.Random.UniformChoose
  exposing
    ( uniformChoose
    )


type alias WallSeed =
  { edgeChance :
    Float
  , connectedBoundary :
    Maybe Direction
  , poles :
    Set
      ( Int
      , Int
      )
  , potentialGrowths :
    List
      ( Position Int
      , Direction
      )
  }


isBoundary : Position Int -> Position Int -> Bool
isBoundary levelSize pos =
  pos.x == 0
    || pos.y == 0
    || pos.x == levelSize.x
    || pos.y == levelSize.y


growWalls : Position Int -> Walls -> List WallSeed -> List WallSeed -> Random.Generator Walls
growWalls levelSize walls deadSeeds liveSeeds =
  case
    liveSeeds
  of
    [] ->
      Random.constant
        walls

    w :: ws ->
      uniformChoose w ws
        |> Random.andThen
          ( \ ( seedToGrow, restSeeds ) ->
            case
              seedToGrow.potentialGrowths
            of
              [] ->
                growWalls levelSize walls (seedToGrow :: deadSeeds) restSeeds

              g :: gs ->
                uniformChoose g gs
                  |> Random.andThen
                    ( \ ( ( oldPole, dir ), restGrowth ) ->
                      Random.float 0 1
                        |> Random.andThen
                          ( \ x ->
                            if
                              x >= seedToGrow.edgeChance
                            then
                              { edgeChance =
                                seedToGrow.edgeChance
                              , connectedBoundary =
                                seedToGrow.connectedBoundary
                              , poles =
                                seedToGrow.poles
                              , potentialGrowths =
                                restGrowth
                              }
                                :: restSeeds
                                |> growWalls levelSize walls deadSeeds
                            else
                              let
                                newPole =
                                  Position.move (Direction.toOffset dir) oldPole
                              in
                                if
                                  isBoundary levelSize newPole
                                then
                                  if
                                    seedToGrow.connectedBoundary /= Nothing
                                  then
                                    { edgeChance =
                                      seedToGrow.edgeChance
                                    , connectedBoundary =
                                      seedToGrow.connectedBoundary
                                    , poles =
                                      seedToGrow.poles
                                    , potentialGrowths =
                                      restGrowth
                                    }
                                      :: restSeeds
                                      |> growWalls levelSize walls deadSeeds
                                  else
                                    let
                                      newWalls =
                                        case
                                          dir
                                        of
                                          Direction.North ->
                                            { horizontal =
                                              walls.horizontal
                                            , vertical =
                                              Set.insert
                                                ( newPole.x
                                                , newPole.y
                                                )
                                                walls.vertical
                                            }
                                          Direction.South ->
                                            { horizontal =
                                              walls.horizontal
                                            , vertical =
                                              Set.insert
                                                ( oldPole.x
                                                , oldPole.y
                                                )
                                                walls.vertical
                                            }
                                          Direction.East ->
                                            { horizontal =
                                              Set.insert
                                                ( oldPole.x
                                                , oldPole.y
                                                )
                                                walls.horizontal
                                            , vertical =
                                              walls.vertical
                                            }
                                          Direction.West ->
                                            { horizontal =
                                              Set.insert
                                                ( newPole.x
                                                , newPole.y
                                                )
                                                walls.horizontal
                                            , vertical =
                                              walls.vertical
                                            }
                                    in
                                      { edgeChance =
                                        seedToGrow.edgeChance
                                      , connectedBoundary =
                                        Just dir
                                      , poles =
                                        Set.insert ( newPole.x, newPole.y ) seedToGrow.poles
                                      , potentialGrowths =
                                        restGrowth
                                      }
                                        :: restSeeds
                                        |> growWalls levelSize newWalls deadSeeds
                                else
                                  if
                                    List.any (.poles >> Set.member ( newPole.x, newPole.y)) deadSeeds
                                      || List.any (.poles >> Set.member ( newPole.x, newPole.y)) liveSeeds
                                  then
                                    { edgeChance =
                                      seedToGrow.edgeChance
                                    , connectedBoundary =
                                      seedToGrow.connectedBoundary
                                    , poles =
                                      seedToGrow.poles
                                    , potentialGrowths =
                                      restGrowth
                                    }
                                      :: restSeeds
                                      |> growWalls levelSize walls deadSeeds
                                  else
                                    let
                                      ( newWalls, newGrowths ) =
                                        case
                                          dir
                                        of
                                          Direction.North ->
                                            ( { horizontal =
                                                walls.horizontal
                                              , vertical =
                                                Set.insert
                                                  ( newPole.x
                                                  , newPole.y
                                                  )
                                                  walls.vertical
                                              }
                                            , List.map
                                              (Tuple.pair newPole)
                                              [ Direction.North
                                              , Direction.East
                                              , Direction.West
                                              ]
                                            )
                                          Direction.South ->
                                            ( { horizontal =
                                                walls.horizontal
                                              , vertical =
                                                Set.insert
                                                  ( oldPole.x
                                                  , oldPole.y
                                                  )
                                                  walls.vertical
                                              }
                                            , List.map
                                              (Tuple.pair newPole)
                                              [ Direction.South
                                              , Direction.East
                                              , Direction.West
                                              ]
                                            )
                                          Direction.East ->
                                            ( { horizontal =
                                                Set.insert
                                                  ( oldPole.x
                                                  , oldPole.y
                                                  )
                                                  walls.horizontal
                                              , vertical =
                                                walls.vertical
                                              }
                                            , List.map
                                              (Tuple.pair newPole)
                                              [ Direction.North
                                              , Direction.South
                                              , Direction.East
                                              ]
                                            )
                                          Direction.West ->
                                            ( { horizontal =
                                                Set.insert
                                                  ( newPole.x
                                                  , newPole.y
                                                  )
                                                  walls.horizontal
                                              , vertical =
                                                walls.vertical
                                              }
                                            , List.map
                                              (Tuple.pair newPole)
                                              [ Direction.North
                                              , Direction.South
                                              , Direction.West
                                              ]
                                            )
                                    in
                                      { edgeChance =
                                        seedToGrow.edgeChance
                                      , connectedBoundary =
                                        Just dir
                                      , poles =
                                        Set.insert ( newPole.x, newPole.y ) seedToGrow.poles
                                      , potentialGrowths =
                                        newGrowths ++ restGrowth
                                      }
                                        :: restSeeds
                                        |> growWalls levelSize newWalls deadSeeds
                          )
                    )
          )


generator : Position Int -> Random.Generator Walls
generator levelSize =
  Random.int 2 6
    |> Random.andThen
      ( \ numSeeds ->
        Random.map2
          Tuple.pair
          ( Random.int 1 (levelSize.x - 1) )
          ( Random.int 1 (levelSize.y - 1) )
          |> Random.list numSeeds
          |> Random.map ( Set.fromList >> Set.toList )
          |> Random.map
            ( List.map
              ( \ ( x, y ) ->
                { edgeChance =
                  0.6
                , connectedBoundary =
                  Nothing
                , poles =
                  Set.singleton ( x, y )
                , potentialGrowths =
                  List.map
                    (Tuple.pair (Position x y))
                    [ Direction.North
                    , Direction.South
                    , Direction.East
                    , Direction.West
                    ]
                }
              )
            )
      )
    |> Random.andThen
      ( growWalls
        levelSize
        { horizontal =
          Set.empty
        , vertical =
          Set.empty
        }
        []
      )
