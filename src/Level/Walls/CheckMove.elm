module Level.Walls.CheckMove
  exposing
    ( checkMove
    )


import Set


import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Position.Type
  exposing
    ( Position
    )
import Util.Direction.Type
  as Direction
  exposing
    ( Direction
    )


checkMove : Walls -> Position Int -> Direction -> Bool
checkMove walls pos dir =
  case
    dir
  of
    Direction.North ->
      Set.member (pos.x, pos.y) walls.horizontal
    Direction.South ->
      Set.member (pos.x, pos.y + 1) walls.horizontal
    Direction.West ->
      Set.member (pos.x, pos.y) walls.vertical
    Direction.East ->
      Set.member (pos.x + 1, pos.y) walls.vertical
