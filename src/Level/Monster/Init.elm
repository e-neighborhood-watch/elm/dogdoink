module Level.Monster.Init
  exposing
    ( init
    )


import Level.Monster.Kind.Type
  as Monster
import Level.Monster.Type
  exposing
    ( Monster
    )


init : Monster.Kind -> Monster
init kind =
  { kind =
    kind
  , animation =
    Nothing
  }
