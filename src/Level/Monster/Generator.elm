module Level.Monster.Generator
  exposing
    ( generator
    )


import Dict
  exposing
    ( Dict
    )
import Random


import Level.Monster.Init
  as Monster
import Level.Monster.Kind.Type
  as Monster
import Level.Monster.Type
  exposing
    ( Monster
    )
import Util.Position.Type
  exposing
    ( Position
    )


addMonster : Position Int -> Position Int -> Dict (Int, Int) Monster-> Random.Generator (Dict (Int, Int) Monster)
addMonster levelSize playerLocation monsterMap =
  List.range 0 (levelSize.x - 1)
  |>
    List.concatMap
      ( \ x ->
        List.range 0 (levelSize.y - 1)
        |>
          List.concatMap
            ( \ y ->
              if
                abs (x - playerLocation.x) + abs (y - playerLocation.y) > 1
                && not (Dict.member (x,y) monsterMap)
              then
                [ ( 1.0
                  , Monster.init Monster.Plus
                    |> Dict.singleton (x, y)
                  )
                , ( 1.0
                  , Monster.init Monster.Circle
                    |> Dict.singleton (x, y)
                  )
                ]
              else
                [
                ]
            )
      )
  |>
    Random.weighted
      ( 0
      , Dict.empty
      )
  |> Random.map
    ( \ newMonsterMap ->
      Dict.union newMonsterMap monsterMap
    )


generator : Int -> Position Int -> Position Int -> Random.Generator (Dict (Int, Int) Monster)
generator score levelSize playerLocation =
  let
    go : Int -> Random.Generator (Dict (Int, Int) Monster)
    go n =
      if
        n < 1
      then
        Random.constant Dict.empty
      else
        go (n - 1)
        |>
          Random.andThen
          ( addMonster levelSize playerLocation )
  in
    go
      ( if
          score < 3
        then
          2
        else if
          score < 8
        then
          3
        else if
          score < 13
        then
          4
        else if
          score < 18
        then
          5
        else
          6
      )
