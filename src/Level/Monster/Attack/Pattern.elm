module Level.Monster.Attack.Pattern
  exposing
    ( pattern
    )


import Level.Monster.Kind.Type
  exposing
    ( Kind (..)
    )
import Level.Monster.Type
  exposing
    ( Monster
    )
import Util.Offset.Type
  exposing
    ( Offset
    )


pattern : Monster -> List (Offset Int)
pattern { kind } =
  case
    kind
  of
    Circle ->
      [ { dx =
          1
        , dy =
          -1
        }
      , { dx =
          1
        , dy =
          0
        }
      , { dx =
          1
        , dy =
          1
        }
      , { dx =
          -1
        , dy =
          -1
        }
      , { dx =
          -1
        , dy =
          0
        }
      , { dx =
          -1
        , dy =
          1
        }
      , { dx =
          0
        , dy =
          1
        }
      , { dx =
          0
        , dy =
          -1
        }
      ]
    Plus ->
      [ { dx =
          0
        , dy =
          0
        }
      , { dx =
          1
        , dy =
          0
        }
      , { dx =
          -1
        , dy =
          0
        }
      , { dx =
          0
        , dy =
          1
        }
      , { dx =
          0
        , dy =
          -1
        }
      ]
