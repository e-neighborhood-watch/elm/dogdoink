module Level.Monster.Kind.Type
  exposing
    ( Kind (..)
    )


type Kind
  = Plus
  | Circle
