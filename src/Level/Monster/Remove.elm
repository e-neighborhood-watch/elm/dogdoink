module Level.Monster.Remove
  exposing
    ( remove
    )


import Dict


import Level.Type
  exposing
    ( Level
    )
import Util.Position.Type
  exposing
    ( Position
    )


remove : Position Int -> Level -> Level
remove pos level =
  { level
  | monsterMap =
    Dict.remove (pos.x, pos.y) level.monsterMap
  }
