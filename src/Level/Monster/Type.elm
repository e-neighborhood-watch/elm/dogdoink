module Level.Monster.Type
  exposing
    ( Monster
    )


import Animated.Type
  exposing
    ( Animated
    )
import Level.Monster.Animation.Type
  as Monster
import Level.Monster.Kind.Type
  as Monster


type alias Monster =
  { animation :
    Maybe (Animated Monster.Animation)
  , kind :
    Monster.Kind
  }
