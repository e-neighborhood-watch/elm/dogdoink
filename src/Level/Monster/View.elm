module Level.Monster.View
  exposing
    ( view
    )


import Dict
  exposing
    ( Dict
    )
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Animated.Completion
  as Animated
import Data.Color.Theme.Type
  as Color
import Level.Monster.Animation.Type
  as Animation
import Level.Monster.Kind.Type
  as Monster
import Level.Monster.Type
  exposing
    ( Monster
    )
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.ToOffset
  as Direction
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Position.Difference
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


view : Settings -> Dict (Int, Int) Monster -> Svg a
view settings monsterDict =
  List.map
    ( \ ( (x, y), monster ) ->
      let
        monsterTransform =
          case
            monster.animation
          of
            Nothing ->
              ""

            Just animation ->
              let
                animationOffset =
                  case
                    animation.kind
                  of
                    Animation.Attack playerPos ->
                      Offset.scale
                        ( Animated.completion animation - 0.5
                        |> (\ a -> 0.25 - a^2)
                        )
                        ( Position.difference (Position x y) playerPos
                        |> Offset.toFloat
                        )

                    Animation.Movement dir ->
                      Offset.scale
                        ( Animated.completion animation
                        |> (*) -1
                        )
                        ( Direction.toOffset dir
                        |> Offset.toFloat
                        )
              in
                "translate("
                  ++ String.fromFloat animationOffset.dx
                  ++ " "
                  ++ String.fromFloat animationOffset.dy
                  ++ ")"
      in
        Svg.use
          [ SvgAttr.xlinkHref
            ( case
                monster.kind
              of
                Monster.Plus ->
                  "#PlusMonster"
                Monster.Circle ->
                  "#CircleMonster"
            )
          , SvgAttr.x (x |> String.fromInt)
          , SvgAttr.y (y |> String.fromInt)
          , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
          , SvgAttr.strokeWidth "0.25"
          , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
          , SvgAttr.transform monsterTransform
          ]
          [
          ]
    )
    ( Dict.toList monsterDict )
    |> Svg.g []

