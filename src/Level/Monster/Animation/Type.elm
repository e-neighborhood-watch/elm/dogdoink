module Level.Monster.Animation.Type
  exposing
    ( Animation (..)
    )


import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Position.Type
  exposing
    ( Position
    )


type Animation
  = Movement Direction
  | Attack (Position Int)
