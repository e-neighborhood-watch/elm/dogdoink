module Level.Adjacents
  exposing
    ( adjacents
    )


import Set
  exposing
    ( Set
    )


import Level.Walls.CheckMove
  as Walls
import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Box
  as Box
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction (..)
    )
import Util.Position.Move
  as Position
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


adjacents : Box Int -> Walls -> Position Int -> Set ( Int, Int )
adjacents levelBox walls position =
  List.filterMap
    ( \ direction ->
      let
        newPosition =
          Position.move
            ( Direction.toOffset direction )
            position
      in
        if
          not (Box.within levelBox newPosition)
            || Walls.checkMove walls position direction
        then
          Nothing
        else
          Position.toPair newPosition
            |> Just
    )
    [ North
    , South
    , East
    , West
    ]
    |> Set.fromList
