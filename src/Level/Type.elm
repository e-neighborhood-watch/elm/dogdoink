module Level.Type
  exposing
    ( Level
    , get
    )


import Dict
  exposing
    ( Dict
    )


import Data.Text.Type
  exposing
    ( Text
    )
import Level.Monster.Type
  exposing
    ( Monster
    )
import Level.Player.Health.Cell.Type
  exposing
    ( Cell
    )
import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Position.Type
  exposing
    ( Position
    )


type alias Level =
  { playerLocation :
    Position Int
  , walls :
    Walls
  , monsterMap :
    Dict (Int, Int) Monster
  , exitLocation :
    Position Int
  , upgradePrizes :
    (Cell, Cell)
  }


get :
  { a
  | playerLocation :
    Position Int
  , walls :
    Walls
  , monsterMap :
    Dict (Int, Int) Monster
  , exitLocation :
    Position Int
  , upgradePrizes :
    (Cell, Cell)
  }
    -> Level
get x =
  { monsterMap =
    x.monsterMap
  , walls =
    x.walls
  , playerLocation =
    x.playerLocation
  , exitLocation =
    x.exitLocation
  , upgradePrizes =
    x.upgradePrizes
  }
