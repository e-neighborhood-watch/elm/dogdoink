module Level.Distances
  exposing
    ( distances
    )


import Array
  exposing
    ( Array
    )
import Set
  exposing
    ( Set
    )


import Level.Adjacents
  as Level
import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.FromPair
  as Position
import Util.Position.ToIndex
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


distancesHelper : Int -> Array Int -> List (Position Int) -> Set ( Int, Int ) -> Box Int -> Walls -> Array Int
distancesHelper distance partialDistances currentDistance nextDistance levelBox walls =
  case
    currentDistance
  of
    [] ->
      case
        nextDistance
          |> Set.toList
          |> List.map Position.fromPair
      of
        [] ->
          partialDistances

        toHandle :: rest ->
          distancesHelper
            (distance+1)
            ( Array.set (Position.toIndex levelBox toHandle) (distance+1) partialDistances )
            rest
            ( Set.filter
              ( \ pos ->
                Array.get
                  ( Position.toIndex levelBox (Position.fromPair pos) )
                  partialDistances
                  == Just -1
              )
              ( Level.adjacents levelBox walls toHandle )
            )
            levelBox
            walls

    toHandle :: rest ->
      distancesHelper
        distance
        (Array.set (Position.toIndex levelBox toHandle) distance partialDistances)
        rest
        ( Set.filter
          ( \ pos ->
            Array.get
              ( Position.toIndex levelBox (Position.fromPair pos) )
              partialDistances
              == Just -1
          )
          ( Level.adjacents levelBox walls toHandle )
          |> Set.union nextDistance
        )
        levelBox
        walls


distances : Box Int -> Position Int -> Walls -> Array Int
distances levelBox location =
  distancesHelper 0 (Array.repeat ((levelBox.max.x - levelBox.min.x) * (levelBox.max.y - levelBox.min.y)) -1) [location] Set.empty levelBox
