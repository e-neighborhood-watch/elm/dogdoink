module Level.Status.Type
  exposing
    ( Status (..)
    , WithStatus
    , add
    )


import Level.Type
  exposing
    ( Level
    )

type Status
  = Complete
  | Incomplete


type alias WithStatus l =
  { l
  | status :
    Status
  }


add : Status -> Level -> WithStatus Level
add status lvl =
  { status =
    status
  , monsterMap =
    lvl.monsterMap
  , walls =
    lvl.walls
  , playerLocation =
    lvl.playerLocation
  , exitLocation =
    lvl.exitLocation
  , upgradePrizes =
    lvl.upgradePrizes
  }
