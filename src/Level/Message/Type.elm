module Level.Message.Type
  exposing
    ( Message (..)
    )


import Dict
  exposing
    ( Dict
    )


import Level.Monster.Animation.Type
  as Monster
import Level.Player.Animation.Type
  as Player
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Position.Type
  exposing
    ( Position
    )


type Message
  = Noop
  | Exit
  | TimePasses Int
  | Move Direction
  | AddAnimations
    { currentTime :
      Int
    , playerAnimation :
      Player.Animation
    , monsterAnimations :
      Dict ( Int, Int ) Monster.Animation
    , damageAnimations :
      List (Position Int)
    , failedAttackAnimation :
      Maybe (Position Int)
    }
