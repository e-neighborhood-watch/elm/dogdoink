module Level.Generator
  exposing
    ( generator
    )


import Dict
  exposing
    ( Dict
    )
import Random
import Set
  exposing
    ( Set
    )


import Level.Monster.Generator
  as Monster
import Level.Monster.Type
  as Monster
  exposing
    ( Monster
    )
import Level.Player.Health.RandomUpgrade
  exposing
    ( randomUpgrades
    )
import Level.Type
  exposing
    ( Level
    )
import Level.Walls.Generator
  as Walls
import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Direction.Type
  exposing
    ( Direction (..)
    )
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


exitGenerator : Position Int -> Position Int -> Random.Generator (Position Int)
exitGenerator levelSize playerLocation =
  levelSize.x * levelSize.y - 1
    |> List.range 0
    |>
      List.filterMap
        ( \ ix ->
            let
              x =
                modBy levelSize.x ix
              y =
                ix // levelSize.x
            in
              if
                abs (x - playerLocation.x) + abs (y - playerLocation.y) > 2
              then
                Just
                  ( 1.0
                  , Position x y
                  )
              else
                Nothing
        )
    |>
      Random.weighted
        ( 0
        , Position 0 0
        )


generator : Int -> Position Int -> Position Int -> Random.Generator Level
generator score levelSize playerLocation =
  Random.map4
    ( Level
      playerLocation
    )
    ( Walls.generator
      levelSize
    )
    ( Monster.generator
      score
      levelSize
      playerLocation
    )
    ( exitGenerator
      levelSize
      playerLocation
    )
    ( randomUpgrades
      score
    )
