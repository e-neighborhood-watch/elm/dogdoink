module Level.Size
  exposing
    ( defaultSize
    )


import Util.Position.Type
  exposing
    ( Position
    )


defaultSize : Position Int
defaultSize =
  { x =
    5
  , y =
    5
  }
