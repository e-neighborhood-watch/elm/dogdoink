module Level.Player.Update.Move
  exposing
    ( move
    )


import Task
import Time


import Level.Message.Type
  as Message
import Level.Model.Type
  as Level
import Level.Player.Health.Step
  as PlayerHealth
import Level.Player.Health.Type
  as Player
import Level.Player.Update.Result.TurnType.Type
  as TurnType
import Level.Player.Update.Result.Type
  as Update
import Util.Box
  as Box
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


move : Box Int -> Level.Model -> Direction -> Update.Result
move levelBox { level, playerAnimation, damageAnimations, failedAttackAnimation, playerHealth, turnsLived, floorsDescended } dir =
  let
    newLocation : Position Int
    newLocation =
      Position.move (Direction.toOffset dir) level.playerLocation

    newPlayerHealth : Player.Health
    newPlayerHealth =
      PlayerHealth.step newLocation playerHealth
  in
    if
      Box.within levelBox newLocation
    then
      Update.TookTurn TurnType.Move
        { level =
          { level
          | playerLocation =
            newLocation
          }
        , playerAnimation =
          playerAnimation
        , damageAnimations =
          damageAnimations
        , failedAttackAnimation =
          failedAttackAnimation
        , playerHealth =
          newPlayerHealth
        , turnsLived =
          turnsLived
        , floorsDescended =
          floorsDescended
        }
    else
      Update.NoTurn TurnType.Move
