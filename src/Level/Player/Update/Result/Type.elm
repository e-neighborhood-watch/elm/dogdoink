module Level.Player.Update.Result.Type
  exposing
    ( Result (..)
    )


import Level.Message.Type
  as Level
import Level.Model.Type
  as Level
import Level.Player.Update.Result.TurnType.Type
  exposing
    ( TurnType
    )


type Result
  = NoTurn TurnType
  | Exit
  | TookTurn TurnType Level.Model
