module Level.Player.Update.Result.TurnType.Type
  exposing
    ( TurnType (..)
    )


type TurnType
  = Move
  | Attack
