module Level.Player.Update.Attack
  exposing
    ( attack
    )


import Task
import Time


import Level.Message.Type
  as Message
import Level.Model.Type
  as Level
import Level.Monster.Remove
  as Monster
import Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )
import Level.Player.Health.Get
  as PlayerHealth
import Level.Player.Health.Set
  as PlayerHealth
import Level.Player.Health.Spend
  as PlayerHealth
import Level.Player.Health.Type
  as Player
import Level.Player.Update.Result.TurnType.Type
  as TurnType
import Level.Player.Update.Result.Type
  as Update
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


attack : Level.Model -> Direction -> Update.Result
attack ({ level, playerHealth } as levelModel) dir =
  let
    targetLocation : Position Int
    targetLocation =
      Position.move (Direction.toOffset dir) level.playerLocation

    postEffectsPlayerHealth =
      case
        PlayerHealth.get level.playerLocation playerHealth
      of
        Just (Fangs n) ->
          PlayerHealth.set targetLocation (Blood 5) playerHealth
        _ ->
          playerHealth
  in
    case
      PlayerHealth.spend level.playerLocation postEffectsPlayerHealth
    of
      Just newPlayerHealth ->
        Update.TookTurn TurnType.Attack
          { levelModel
          | playerHealth =
            newPlayerHealth
          , level =
            Monster.remove targetLocation level
          }
      Nothing ->
        Update.NoTurn TurnType.Attack
