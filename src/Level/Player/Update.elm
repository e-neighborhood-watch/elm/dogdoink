module Level.Player.Update
  exposing
    ( update
    )


import Dict


import Level.Model.Type
  as Level
import Level.Player.Update.Attack
  as Player
import Level.Player.Update.Move
  as Player
import Level.Player.Update.Result.Type
  as Update
import Level.Player.Update.Result.TurnType.Type
  as TurnType
import Level.Walls.CheckMove
  as Walls
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction
    )
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


update : Box Int -> Direction -> Level.Model -> Update.Result
update levelBox dir ({ level, playerHealth } as model) =
  if
    level.playerLocation == level.exitLocation
  then
    Update.Exit
  else if
    Walls.checkMove level.walls level.playerLocation dir
  then
    Update.NoTurn TurnType.Move
  else
    let
      newLocation : Position Int
      newLocation =
        Position.move (Direction.toOffset dir) level.playerLocation
    in
      if
        Dict.member (newLocation.x, newLocation.y) level.monsterMap
      then
        Player.attack model dir
      else
        Player.move levelBox model dir
