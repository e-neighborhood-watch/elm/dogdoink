module Level.Player.Health.View
  exposing
    ( view
    )


import Array
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Animated.Completion
  as Animated
import Animated.Type
  exposing
    ( Animated
    )
import Data.Color.Theme.Type
  as Color
import Level.Player.Health.Cell.View
  as Cell
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Settings.Color.Theme
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Type
  exposing
    ( Settings
    )
import Util.Position.Type
  exposing
    ( Position
    )


viewHealth : Settings -> Int -> Health -> Svg a
viewHealth settings verticalOffset { width, healths } =
  healths
    |> Array.toIndexedList
    |>
      List.concatMap
        ( \ ( ix, cell ) ->
          let
            pos =
              { x =
                ix
                  |> modBy width
                  |> toFloat
                  |> (+) 0.3
              , y =
                ix // width
                  |> (+) verticalOffset
                  |> toFloat
                  |> (+) 1.3
              }
          in
            Cell.view settings pos cell
        )
    |> Svg.g []


viewDamage : Settings -> List (Animated (Position Int)) -> Int -> Svg a
viewDamage settings animations verticalOffset =
  animations
    |> List.map
      ( \ animation ->
        Svg.rect
          [ animation.kind.x
            |> toFloat
            |> (+) 0.15
            |> String.fromFloat
            |> SvgAttr.x
          , animation.kind.y
            |> (+) verticalOffset
            |> toFloat
            |> (+) 1.15
            |> String.fromFloat
            |> SvgAttr.y
          , SvgAttr.rx "0.14"
          , SvgAttr.ry "0.14"
          , Animated.completion animation
            |> (-) 0.5
            |> abs
            |> (-) 0.5
            |> String.fromFloat
            |> SvgAttr.opacity
          , SvgAttr.width "0.86"
          , SvgAttr.height "0.86"
          , SvgAttr.fill (Theme.toHex settings.theme Color.Damage)
          ]
          []
      )
    |> Svg.g []


viewFailedAttack : Settings -> Int -> Maybe (Animated (Position Int)) ->  Svg a
viewFailedAttack settings verticalOffset maybeAnimation =
  case
    maybeAnimation
  of
    Nothing ->
      Svg.g [] []

    Just animation ->
      Svg.use
        [ SvgAttr.xlinkHref "#Die"
        , SvgAttr.x (animation.kind.x |> toFloat |> (+) 0.3 |> String.fromFloat)
        , SvgAttr.y (animation.kind.y + verticalOffset |> toFloat |> (+) 1.3 |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Blood)
        , SvgAttr.strokeWidth "0.4"
        , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
          , Animated.completion animation
            |> (-) 0.5
            |> abs
            |> (-) 0.5
            |> (*) 2
            |> String.fromFloat
            |> SvgAttr.opacity
        ]
        []


view : Settings -> List (Animated (Position Int)) -> Maybe (Animated (Position Int)) -> Int -> Health -> Svg a
view settings damageAnimations failedAttackAnimation verticalOffset health =
  Svg.g
    []
    [ viewHealth settings verticalOffset health
    , viewFailedAttack settings verticalOffset failedAttackAnimation
    , viewDamage settings damageAnimations verticalOffset
    ]
