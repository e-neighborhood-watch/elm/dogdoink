module Level.Player.Health.Spend
  exposing
    ( spend
    )


import Array


import Level.Player.Health.Cell.Spend
  as Cell
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Util.Position.Type
  exposing
    ( Position
    )


spend : Position Int -> Health -> Maybe Health
spend {x, y} {width, healths } =
  let
    index : Int
    index = x + y * width
  in
    if
      x < width && x >= 0
    then
      Array.get index healths
        |> Maybe.andThen
          ( \ cell ->
            Cell.spend cell
            |> Maybe.map
              ( \ newCell ->
                Array.set index newCell healths
              )
          )
      |> Maybe.map ( Health width )
    else
      Nothing
