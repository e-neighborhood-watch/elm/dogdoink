module Level.Player.Health.Type
  exposing
    ( Health
    )


import Array
  exposing
    ( Array
    )


import Level.Player.Health.Cell.Type
  exposing
    ( Cell
    )


type alias Health =
  { width :
    Int
  , healths :
    Array Cell
  }
