module Level.Player.Health.Alive
  exposing
    ( alive
    )


import Array


import Level.Player.Health.Cell.Vital
  as Cell
import Level.Player.Health.Type
  exposing
    ( Health
    )
    

alive : Health -> Bool
alive { healths } =
  List.any Cell.vital (Array.toList healths)
