module Level.Player.Health.Finish
  exposing
    ( finish
    )


import Array


import Level.Player.Health.Cell.Grow
  as Cell
import Level.Player.Health.Type
  exposing
    ( Health
    )


finish : Health -> Health
finish health =
  { health
  | healths =
    Array.map Cell.grow health.healths
  }

