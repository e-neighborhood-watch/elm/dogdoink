module Level.Player.Health.Get
  exposing
    ( get
    )


import Array


import Level.Player.Health.Cell.Type
  exposing
    ( Cell
    )
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Util.Position.Type
  exposing
    ( Position
    )


get : Position Int -> Health -> Maybe Cell
get { x, y } { width, healths } =
  Array.get (x + y*width) healths
