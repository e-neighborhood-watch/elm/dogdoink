module Level.Player.Health.Step
  exposing
    ( step
    )


import Level.Player.Health.Cell.Step
  as Cell
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Level.Player.Health.Update
  as Health
import Util.Position.Type
  exposing
    ( Position
    )


step : Position Int -> Health -> Health
step pos =
  Health.update pos Cell.step
