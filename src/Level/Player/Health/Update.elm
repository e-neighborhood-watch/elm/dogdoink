module Level.Player.Health.Update
  exposing
    ( update
    )


import Array


import Level.Player.Health.Cell.Type
  exposing
    ( Cell
    )
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Util.Position.Type
  exposing
    ( Position
    )


update : Position Int -> (Cell -> Cell) -> Health -> Health
update { x, y } f { width, healths } =
  let
    index : Int
    index = x + y * width
  in
    { width =
      width
    , healths =
      if
        x < width && x >= 0
      then
        Array.get index healths
          |> Maybe.map ( \ h -> Array.set index (f h) healths )
          |> Maybe.withDefault healths
      else
        healths
    }
