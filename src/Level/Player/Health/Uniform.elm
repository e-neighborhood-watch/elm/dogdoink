module Level.Player.Health.Uniform
  exposing
    ( uniform
    )


import Array


import Level.Player.Health.Cell.Type
  exposing
    ( Cell
    )
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Util.Position.Type
  exposing
    ( Position
    )


uniform : Position Int -> Cell -> Health
uniform pos cell =
  { width =
    pos.x
  , healths =
    Array.repeat (pos.x * pos.y) cell
  }
