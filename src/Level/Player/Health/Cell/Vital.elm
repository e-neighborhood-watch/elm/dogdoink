module Level.Player.Health.Cell.Vital
  exposing
    ( vital
    )

import Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )

vital : Cell -> Bool
vital cell =
  case
    cell
  of
    Blood n ->
      n > 0
    Shield n ->
      n > 0
    Fungus n ->
      n > 0
    _ ->
      False
      
