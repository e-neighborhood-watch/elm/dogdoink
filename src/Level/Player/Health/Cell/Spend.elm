module Level.Player.Health.Cell.Spend
  exposing
    ( spend
    )


import Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )


spend : Cell -> Maybe Cell
spend cell =
  case
    cell
  of
    Blood n ->
      if
        n < 1
      then
        -- Should never happen
        Nothing
      else if
        n == 1
      then
        Just Empty
      else
        Just (Blood (n - 1))

    Fungus n ->
      if
        n < 1
      then
        -- Should never happen
        Nothing
      else if
        n == 1
      then
        Just Empty
      else
        Just (Fungus (n - 1))

    Shield n ->
      if
        n < 1
      then
        -- Should never happen
        Nothing
      else if
        n == 1
      then
        Just Empty
      else
        Just (Shield (n - 1))

    Sword ->
      Just Sword

    Empty ->
      Nothing

    Fangs n ->
      if
        n < 1
      then
        -- Should never happen
        Nothing
      else if
        n == 1
      then
        Just Empty
      else
        Just (Fangs (n - 1))
