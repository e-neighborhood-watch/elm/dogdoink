module Level.Player.Health.Cell.Step
  exposing
    ( step
    )


import Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )


step : Cell -> Cell
step cell =
  case
    cell
  of
    Blood n ->
      if
        n < 2
      then
        Empty
      else
        Blood (n - 1)

    Fungus n ->
      if
        n < 2
      then
        Empty
      else
        Fungus (n - 1)

    Sword ->
      Sword

    Shield n ->
      Shield n

    Empty ->
      Empty

    Fangs n ->
      Fangs n
