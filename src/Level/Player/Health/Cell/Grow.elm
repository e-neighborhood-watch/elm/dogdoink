module Level.Player.Health.Cell.Grow
  exposing
    ( grow
    )


import Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )


grow : Cell -> Cell
grow cell =
  case
    cell
  of
    Fungus n ->
      if
        n < 9
      then
        Fungus (n + 1)
      else
        Fungus 9

    _ ->
      cell
