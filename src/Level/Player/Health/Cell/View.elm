module Level.Player.Health.Cell.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Level.Player.Health.Cell.Type
  as Cell
  exposing
    ( Cell
    )
import Level.Player.Health.Cell.Vital
  as Cell
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Position.Type
  exposing
    ( Position
    )


viewPip : String -> String -> Float -> Float -> Svg a
viewPip icon color x y =
  Svg.use
    [ SvgAttr.x (String.fromFloat x)
    , SvgAttr.y (String.fromFloat y)
    , SvgAttr.xlinkHref icon
    , SvgAttr.fill color
    ]
    [
    ]


viewPips : String -> String -> Float -> Float -> Int -> Svg a
viewPips icon color x y count =
  if
    count > 0
  then
    Svg.svg
      [ SvgAttr.viewBox "0 0 9 9"
      , SvgAttr.x (String.fromFloat x)
      , SvgAttr.y (String.fromFloat y)
      , SvgAttr.width "0.6"
      , SvgAttr.height "0.6"
      ]
      ( case
        count
      of
        1 ->
          [ viewPip icon color 3 3
          ]
        2 ->
          [ viewPip icon color 0 0
          , viewPip icon color 6 6
          ]
        3 ->
          [ viewPip icon color 0 6
          , viewPip icon color 6 0
          , viewPip icon color 3 3
          ]
        4 ->
          [ viewPip icon color 0 6
          , viewPip icon color 6 0
          , viewPip icon color 0 0
          , viewPip icon color 6 6
          ]
        5 ->
          [ viewPip icon color 0 0
          , viewPip icon color 6 0
          , viewPip icon color 6 6
          , viewPip icon color 0 6
          , viewPip icon color 3 3
          ]
        6 ->
          [ viewPip icon color 0 6
          , viewPip icon color 0 3
          , viewPip icon color 0 0
          , viewPip icon color 6 0
          , viewPip icon color 6 3
          , viewPip icon color 6 6
          ]
        7 ->
          [ viewPip icon color 0 6
          , viewPip icon color 0 3
          , viewPip icon color 0 0
          , viewPip icon color 6 0
          , viewPip icon color 6 3
          , viewPip icon color 6 6
          , viewPip icon color 3 3
          ]
        8 ->
          [ viewPip icon color 0 6
          , viewPip icon color 0 3
          , viewPip icon color 0 0
          , viewPip icon color 6 0
          , viewPip icon color 6 3
          , viewPip icon color 6 6
          , viewPip icon color 3 0
          , viewPip icon color 3 6
          ]
        9 ->
          [ viewPip icon color 0 6
          , viewPip icon color 0 3
          , viewPip icon color 0 0
          , viewPip icon color 6 0
          , viewPip icon color 6 3
          , viewPip icon color 6 6
          , viewPip icon color 3 0
          , viewPip icon color 3 6
          , viewPip icon color 3 3
          ]
        _ ->
          []
      )
  else
    -- Only drawn when there is a bug
    Svg.use
      [ SvgAttr.xlinkHref "#BadPips"
      , SvgAttr.x (x |> String.fromFloat)
      , SvgAttr.y (y |> String.fromFloat)
      , SvgAttr.fill "#FF0000"
      ]
      [
      ]

view : Settings -> Position Float -> Cell -> List (Svg a)
view settings pos cell =
  if
    cell == Cell.Empty
  then
    []
  else
    [ Svg.use
          [ SvgAttr.xlinkHref "#Die"
          , SvgAttr.x (pos.x |> String.fromFloat)
          , SvgAttr.y (pos.y |> String.fromFloat)
          , SvgAttr.stroke (Theme.toHex settings.theme
              ( if
                  Cell.vital cell
                then
                  Color.Blood
                else
                  Color.Foreground
              )
            )
          , SvgAttr.strokeWidth "0.4"
          , SvgAttr.fill (Theme.toHex settings.theme Color.Background)
          ]
          [
          ]
      , ( case
            cell
          of
            Cell.Sword ->
               Svg.use
                [ SvgAttr.xlinkHref "#Sword"
                , SvgAttr.x (pos.x |> String.fromFloat)
                , SvgAttr.y (pos.y |> String.fromFloat)
                , SvgAttr.fill (Theme.toHex settings.theme Color.Foreground)
                , SvgAttr.stroke "none"
                ]
                [
                ]
            Cell.Shield n ->
              viewPips "#Shield" (Theme.toHex settings.theme Color.Foreground) pos.x pos.y n
            Cell.Blood n ->
              viewPips "#Drop" (Theme.toHex settings.theme Color.Blood) pos.x pos.y n
            Cell.Fangs n ->
              viewPips "#Fang" (Theme.toHex settings.theme Color.Foreground) pos.x pos.y n
            Cell.Fungus n ->
              viewPips "#Mushroom" (Theme.toHex settings.theme Color.Growth) pos.x pos.y n
            Cell.Empty ->
              -- Literally impossible
              viewPips "#Drop" (Theme.toHex settings.theme Color.Blood) pos.x pos.y 0
        )
    ]
