module Level.Player.Health.Cell.Hit
  exposing
    ( hit
    )


import Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )


hit : Cell -> Cell
hit cell =
  case
    cell
  of
    Blood n ->
      if
        n < 2
      then
        Empty
      else
        Blood (n - 1)

    Fungus n ->
      if
        n < 2
      then
        Empty
      else
        Fungus (n - 1)

    Sword ->
      Empty

    Shield n ->
      if
        n < 2
      then
        Empty
      else
        Shield (n - 1)

    Empty ->
      Empty

    Fangs n ->
      if
        n < 2
      then
        Empty
      else
        Fangs (n - 1)
