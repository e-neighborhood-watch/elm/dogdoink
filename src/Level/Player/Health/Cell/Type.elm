module Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )


type Cell
  = Blood Int
  | Sword
  | Shield Int
  | Fangs Int
  | Empty
  | Fungus Int
