module Level.Player.Health.Cell.Default
  exposing
    ( default
    )


import Level.Player.Health.Cell.Type
  as Cell
  exposing
    ( Cell
    )

default : Cell
default =
  Cell.Blood 5
