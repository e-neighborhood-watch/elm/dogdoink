module Level.Player.Health.Set
  exposing
    ( set
    )


import Array


import Level.Player.Health.Cell.Type
  exposing
    ( Cell
    )
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Util.Position.Type
  exposing
    ( Position
    )


set : Position Int -> Cell -> Health -> Health
set { x, y } cell { width, healths } =
  let
    index : Int
    index = x + y * width
  in
    { width =
      width
    , healths =
      if
        x < width && x >= 0
      then
        Array.set index cell healths
      else
        healths
    }
