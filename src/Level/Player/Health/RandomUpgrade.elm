module Level.Player.Health.RandomUpgrade
  exposing
    ( randomUpgrades
    )


import Random
  exposing
    ( uniform
    )


import Level.Player.Health.Cell.Type
  exposing
    ( Cell (..)
    )


randomUpgrade : Int -> Random.Generator Cell
randomUpgrade size =
    Random.uniform Sword
      [ Shield size
      , Fangs size
      , Fungus size
      , Blood 7
      ]


randomSize : Int -> Random.Generator Int
randomSize score =
  if
    score < 5
  then
    Random.constant 3
  else
    Random.uniform 3
      [ 5
      ]


randomUpgrades : Int -> Random.Generator (Cell, Cell)
randomUpgrades score =
  let
    go size upgrade1 =
      randomUpgrade size
      |>
        Random.andThen
          ( \ upgrade2 ->
            if
              upgrade1 == upgrade2
            then
              go size upgrade1
            else
              Random.constant (upgrade1, upgrade2)
          )
  in
    randomSize score
    |> Random.andThen
      ( \ size ->
        randomUpgrade size
        |> Random.andThen (go size)
      )

