module Level.Player.Animation.Type
  exposing
    ( Animation (..)
    )


import Animated.Type
  exposing
    ( Animated
    )
import Util.Direction.Type
  exposing
    ( Direction
    )


type Animation
  = Movement Direction
  | Attack Direction
  | FailedMovement Direction
