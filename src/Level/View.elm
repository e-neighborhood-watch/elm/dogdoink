module Level.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Animated.Completion
  as Animated
import Data.Color.Theme.Type
  as Color
import Data.Text.Type
  as Text
import Data.Font.Size
  as Font
import Document.Type
  exposing
    ( Document
    )
import Level.Gridlines.View
  as Gridlines
import Level.Message.Type
  exposing
    ( Message
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Monster.View
  as Monster
import Level.Player.Animation.Type
  as PlayerAnimation
import Level.Player.Health.View
  as PlayerHealth
import Level.Walls.View
  as Walls
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  as Settings
  exposing
    ( Settings
    )
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Direction.ToOffset
  as Direction
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )


view : Settings -> Box Int -> Model -> Document Message
view settings levelBox { level, playerAnimation, damageAnimations, failedAttackAnimation, playerHealth } =
  let
    playerTransform : String
    playerTransform =
      case
        playerAnimation
      of
        Nothing ->
          ""

        Just animation ->
          let
            animationOffset : Offset Float
            animationOffset =
              case
                animation.kind
              of
                PlayerAnimation.Movement dir ->
                  Offset.scale
                    ( Animated.completion animation
                    |> (*) -1
                    )
                    ( Direction.toOffset dir
                    |> Offset.toFloat
                    )
                PlayerAnimation.Attack dir ->
                  Offset.scale
                    ( Animated.completion animation - 0.5
                    |> (\ x -> 0.25 - x^2)
                    )
                    ( Direction.toOffset dir
                    |> Offset.toFloat
                    )
                PlayerAnimation.FailedMovement dir ->
                  Offset.scale
                    ( Animated.completion animation - 0.5
                    |> (\ x -> 0.25 - x^2)
                    )
                    ( Direction.toOffset dir
                    |> Offset.toFloat
                    )

          in
            "translate("
              ++ String.fromFloat animationOffset.dx
              ++ " "
              ++ String.fromFloat animationOffset.dy
              ++ ")"
    viewBox =
      { min =
        { x =
          -0.6
        , y =
          -0.6
        }
      , max =
        { x =
          toFloat levelBox.max.x + 1.2
        , y =
          toFloat (levelBox.max.y * 2) + 2.5
        }
      }
    rectBox =
      { min =
        { x =
          0.0
        , y =
          0.0
        }
      , max =
        { x =
          toFloat levelBox.max.x
        , y =
          toFloat levelBox.max.y
        }
      }
  in
    { title =
      Language.fromText settings.language Text.LevelTitle
    , viewBox =
      viewBox
    , body =
      [ Gridlines.view settings levelBox
      , Svg.rect
        [ SvgAttr.x (rectBox.min.x |> String.fromFloat)
        , SvgAttr.y (rectBox.min.y |> String.fromFloat)
        , SvgAttr.fill "none"
        , SvgAttr.width (rectBox.max.x - rectBox.min.x |> String.fromFloat)
        , SvgAttr.height (rectBox.max.y - rectBox.min.y |> String.fromFloat)
        , SvgAttr.stroke (Theme.toHex settings.theme Color.Foreground)
        , SvgAttr.strokeWidth "0.1"
        ]
        []
      , Svg.use
        [ SvgAttr.xlinkHref "#Exit"
        , SvgAttr.x (level.exitLocation.x |> String.fromInt)
        , SvgAttr.y (level.exitLocation.y |> String.fromInt)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Accent)
        ]
        [
        ]
      , Svg.use
        [ SvgAttr.xlinkHref "#Character"
        , SvgAttr.x (level.playerLocation.x |> String.fromInt)
        , SvgAttr.y (level.playerLocation.y |> String.fromInt)
        , SvgAttr.fill (Theme.toHex settings.theme Color.Blood)
        , SvgAttr.transform playerTransform
        ]
        [
        ]
      , Monster.view settings level.monsterMap
      , PlayerHealth.view settings damageAnimations failedAttackAnimation levelBox.max.y playerHealth
      , Walls.view settings levelBox level.walls
      ]
    }
