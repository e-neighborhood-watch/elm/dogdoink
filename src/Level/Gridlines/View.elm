module Level.Gridlines.View
  exposing
    ( view
    )


import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Settings.Color.Theme
  as Theme
import Settings.Type
  exposing
    ( Settings
    )
import Util.Box.Type
  exposing
    ( Box
    )


view : Settings -> Box Int -> Svg a
view { theme, drawGridlines } levelBox =
  if
    drawGridlines
  then
    Svg.g
      [
      ]
      [ Svg.g
        [
        ]
        ( List.map
          ( \ x ->
            Svg.line
              [ SvgAttr.x1 (x |> String.fromInt)
              , SvgAttr.y1 (levelBox.min.y |> String.fromInt)
              , SvgAttr.x2 (x |> String.fromInt)
              , SvgAttr.y2 (levelBox.max.y |> String.fromInt)
              , SvgAttr.stroke (Theme.toHex theme Color.Accent)
              , SvgAttr.strokeWidth "0.05"
              , SvgAttr.strokeDasharray "0.1"
              , SvgAttr.strokeDashoffset "0.05"
              ]
              [
              ]
          )
          ( List.range levelBox.min.x levelBox.max.x )
        )
      , Svg.g
        [
        ]
        ( List.map
          ( \ y ->
            Svg.line
              [ SvgAttr.y1 (y |> String.fromInt)
              , SvgAttr.x1 (levelBox.min.x |> String.fromInt)
              , SvgAttr.y2 (y |> String.fromInt)
              , SvgAttr.x2 (levelBox.max.x |> String.fromInt)
              , SvgAttr.stroke (Theme.toHex theme Color.Accent)
              , SvgAttr.strokeWidth "0.05"
              , SvgAttr.strokeDasharray "0.1"
              , SvgAttr.strokeDashoffset "0.05"
              ]
              [
              ]
          )
          ( List.range levelBox.min.y levelBox.max.y )
        )
      ]
  else
    Svg.g [] []
