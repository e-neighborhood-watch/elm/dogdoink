module Level.Update.Result.Type
  exposing
    ( Result (..)
    )


import Animated.Type
  exposing
    ( Animated
    )
import Level.Message.Type
  as Level
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Player.Health.Type
  exposing
    ( Health
    )
import Util.Position.Type
  exposing
    ( Position
    )


type Result
  = NoChange (Cmd Level.Message)
  | InternalChange Model (Cmd Level.Message)
  | Win Health (List (Animated (Position Int)))
  | Exit
  | Defeat Model
