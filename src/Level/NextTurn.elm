module Level.NextTurn
  exposing
    ( nextTurn
    )


import Array
  exposing
    ( Array
    )
import Dict
  exposing
    ( Dict
    )
import Set
  exposing
    ( Set
    )


import Level.Distances
  as Level
import Level.Message.Type
  exposing
    ( Message (..)
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Monster.Animation.Type
  as Monster
import Level.Monster.Attack.Pattern
  as Monster
import Level.Monster.Type
  exposing
    ( Monster
    )
import Level.Player.Health.Cell.Hit
  as Cell
import Level.Player.Health.Cell.Type
  as Cell
import Level.Player.Health.Get
  as Health
import Level.Player.Health.Update
  as Health
import Level.Walls.CheckMove
  as Walls
import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Box
  as Box
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Direction.FromOffset
  as Direction
import Util.Direction.ToOffset
  as Direction
import Util.Direction.Type
  exposing
    ( Direction (..)
    )
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.FromPair
  as Position
import Util.Position.Difference
  as Position
import Util.Position.Move
  as Position
import Util.Position.ToIndex
  as Position
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


type ActionKind
  = Attack
  | Move Direction
  | Wait


type alias Action =
  { localUtility :
    Int
  , kind :
    ActionKind
  , location :
    ( Int, Int )
  }

type alias MonsterAction =
  { monster :
    Monster
  , firstAction :
    Action
  , restActions :
    List Action
  , action :
    Action
  , remainingActions :
    List Action
  }


incrementActions : List MonsterAction -> Maybe (List MonsterAction)
incrementActions actions =
  case
    actions
  of
    [] ->
      Nothing

    t :: ts ->
      case
        t.remainingActions
      of
        [] ->
          incrementActions ts
            |> Maybe.map
              ( (::)
                { monster =
                  t.monster
                , firstAction =
                  t.firstAction
                , restActions =
                  t.restActions
                , action =
                  t.firstAction
                , remainingActions =
                  t.restActions
                }
              )

        newAction :: rest ->
            { monster =
              t.monster
            , firstAction =
              t.firstAction
            , restActions =
              t.restActions
            , action =
              newAction
            , remainingActions =
              rest
            }
              :: ts
              |> Just


evaluateActionsHelper : Int -> Set ( Int, Int )-> List MonsterAction -> Maybe Int
evaluateActionsHelper utility occupied list =
  case
    list
  of
    [] ->
      Just utility

    ma :: mas ->
      if
        Set.member ma.action.location occupied
      then
        Nothing
      else
        evaluateActionsHelper (utility + ma.action.localUtility) (Set.insert ma.action.location occupied) mas


evaluateActions : List MonsterAction -> Maybe Int
evaluateActions =
  evaluateActionsHelper 0 Set.empty


bestActionsHelper : Maybe Int -> List MonsterAction -> Maybe (List MonsterAction) -> List MonsterAction
bestActionsHelper bestUtility theBestActions currentActions =
  case
    currentActions
  of
    Nothing ->
      theBestActions

    Just actions ->
      incrementActions actions
        |>
          case
            evaluateActions actions
          of
            Nothing ->
              bestActionsHelper bestUtility theBestActions

            Just newUtility ->
              case
                bestUtility
              of
                Nothing ->
                  bestActionsHelper (Just newUtility) actions

                Just prevUtility ->
                  if
                    newUtility < prevUtility
                  then
                    bestActionsHelper (Just newUtility) actions
                  else
                    bestActionsHelper bestUtility theBestActions


createMonsterAction : Box Int -> Position Int -> Walls -> Array Int -> ( ( Int, Int ), Monster ) -> MonsterAction
createMonsterAction levelBox playerLocation walls distances ( pos, monster ) =
  let
    currentPosition =
      Position.fromPair pos

    currentIndex =
      Position.toIndex levelBox currentPosition

    waitAction =
      { localUtility =
        Array.get
          currentIndex
          distances
          |> Maybe.withDefault 100 -- this should never happen
          |> (*) 2
      , location =
        pos
      , kind =
        Wait
      }

    moveActions =
      List.filterMap
        ( \ direction ->
          let
            newPosition =
              Position.move
                ( Direction.toOffset direction )
                currentPosition
          in
            if
              Walls.checkMove walls currentPosition direction
                || newPosition == playerLocation
                || not (Box.within levelBox newPosition)
            then
              Nothing
            else
              let
                newIndex =
                  Position.toIndex levelBox newPosition
              in
                Array.get newIndex distances
                  |> Maybe.map
                    ( \ localUtility ->
                      { localUtility =
                        localUtility * 2
                      , location =
                        Position.toPair newPosition
                      , kind =
                        Move direction
                      }
                    )
        )
        [ North
        , South
        , East
        , West
        ]

    otherActions =
      case
        Direction.fromOffset (Position.difference currentPosition playerLocation)
      of
        Nothing ->
          moveActions

        Just dir ->
          if
            Walls.checkMove walls currentPosition dir
          then
            moveActions
          else
            { localUtility =
              1
            , location =
              pos
            , kind =
              Attack
            }
              :: moveActions
  in
    { monster =
      monster
    , firstAction =
      waitAction
    , restActions =
      otherActions
    , action =
      waitAction
    , remainingActions =
      otherActions
    }


bestActions : Box Int -> Position Int -> Walls -> List ( ( Int, Int ), Monster ) -> List ( ( Int, Int ), MonsterAction )
bestActions levelBox playerLocation walls monsters =
  let
    initialActions =
      List.map
        ( createMonsterAction levelBox playerLocation walls (Level.distances levelBox playerLocation walls))
        monsters
  in
    List.map2
      ( always >> Tuple.mapSecond )
      ( bestActionsHelper
        ( evaluateActions initialActions )
        initialActions
        ( incrementActions initialActions )
      )
      monsters


processActions : Position Int -> List ( (Int, Int), MonsterAction ) -> Dict ( Int, Int ) Monster -> Dict ( Int, Int ) Monster.Animation -> List (Offset Int) -> ( Dict ( Int, Int ) Monster, Dict ( Int, Int ) Monster.Animation, List (Offset Int) )
processActions playerLocation list monsterMap animations damage = 
  case
    list
  of
    [] ->
      ( monsterMap
      , animations
      , damage
      )
    ((x, y), ma) :: mas ->
      case
        ma.action.kind
      of
        Wait ->
          processActions
            playerLocation
            mas
            ( Dict.insert ma.action.location ma.monster monsterMap )
            animations
            damage

        Move dir ->
          processActions
            playerLocation
            mas
            ( Dict.insert ma.action.location ma.monster monsterMap )
            ( Dict.insert ma.action.location (Monster.Movement dir) animations )
            damage

        Attack ->
          processActions
            playerLocation
            mas
            ( Dict.insert ma.action.location ma.monster monsterMap )
            ( Dict.insert ma.action.location (Monster.Attack playerLocation) animations )
            ( Monster.pattern ma.monster ++ damage )


nextTurn : Box Int -> Model -> ( Model, Dict ( Int, Int ) Monster.Animation, List (Position Int) )
nextTurn levelBox ({ level, playerHealth } as model) =
  let
    actions =
      Dict.toList level.monsterMap
        |> bestActions levelBox level.playerLocation level.walls

    ( monsterMap, animations, relativeDamage ) =
      processActions level.playerLocation actions Dict.empty Dict.empty []

    damage =
      List.filterMap
        ( \ offset ->
          let
            position =
              Position.move offset level.playerLocation
          in
            if
              Box.within levelBox position
            then
              Just position
            else
              Nothing
        )
      relativeDamage
  in
    ( { model
      | level =
        { level
        | monsterMap =
          monsterMap
        }
      , playerHealth =
        case
          Health.get level.playerLocation playerHealth
        of
          Just (Cell.Shield _) ->
            if
              List.member level.playerLocation damage
            then
              Health.update level.playerLocation Cell.hit playerHealth
            else
              playerHealth

          _ ->
            List.foldr
              ( \ target ->
                Health.update
                  target
                  Cell.hit
              )
              playerHealth
              damage
      }
    , animations
    , damage
    )
