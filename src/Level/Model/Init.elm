module Level.Model.Init
  exposing
    ( init
    )


import Level.Model.Type
  exposing
    ( Model
    )
import Level.Player.Health.Type
  as Player
import Level.Type
  exposing
    ( Level
    )
import Util.Position.Type
  exposing
    ( Position
    )


init : Player.Health -> Level -> Model
init playerHealth level =
  { level =
    level
  , playerAnimation =
    Nothing
  , damageAnimations =
    []
  , failedAttackAnimation =
    Nothing
  , playerHealth =
    playerHealth
  , turnsLived =
    0
  , floorsDescended =
    0
  }

