module Level.Model.Type
  exposing
    ( Model
    )


import Animated.Type
  exposing
    ( Animated
    )
import Level.Player.Animation.Type
  as Player
import Level.Player.Health.Type
  as Player
import Level.Type
  exposing
    ( Level
    )
import Util.Position.Type
  exposing
    ( Position
    )


type alias Model =
  { level :
    Level
  , playerAnimation :
    Maybe (Animated Player.Animation)
  , damageAnimations :
    List (Animated (Position Int))
  , failedAttackAnimation :
    Maybe (Animated (Position Int))
  , playerHealth :
    Player.Health
  , turnsLived :
    Int
  , floorsDescended :
    Int
  }
