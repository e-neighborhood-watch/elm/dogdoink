module Level.Update
  exposing
    ( update
    )


import Dict
  exposing
    ( Dict
    )
import Task
import Time


import Animated.Delayed
  as Animated
import Animated.Immediate
  as Animated
import Animated.Update
  as Animated
import Level.Message.Type
  as Message
  exposing
    ( Message
    )
import Level.Animation.Update
  as Animation
import Level.Cornered
  as Level
import Level.Model.Type
  exposing
    ( Model
    )
import Level.Monster.Animation.Type
  as Monster
import Level.Player.Animation.Type
  as Player
import Level.Player.Health.Alive
  as PlayerHealth
import Level.Player.Health.Type
  as Player
import Level.Player.Update
  as Player
import Level.Player.Update.Result.TurnType.Type
  as TurnType
import Level.Player.Update.Result.Type
  as PlayerUpdate
import Level.NextTurn
  as Level
import Level.Update.Result.Type
  as Update
import Level.Walls.CheckMove
  as Walls
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Offset.Scale
  as Offset
import Util.Offset.ToFloat
  as Offset
import Util.Offset.Type
  exposing
    ( Offset
    )
import Util.Position.Move
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


animationsCommand : Dict ( Int, Int ) Monster.Animation -> List (Position Int) -> Maybe (Position Int) -> Player.Animation -> Cmd Message
animationsCommand monsterAnimations damageAnimations failedAttackAnimation playerAnimation =
  Time.now
    |> Task.perform
      ( \ posix ->
        Message.AddAnimations
          { currentTime =
            Time.posixToMillis posix
          , playerAnimation =
            playerAnimation
          , damageAnimations =
            damageAnimations
          , monsterAnimations =
            monsterAnimations
          , failedAttackAnimation =
            failedAttackAnimation
          }
      )


update : Message -> Box Int -> Model -> Update.Result
update msg levelBox ({ level, turnsLived, floorsDescended, playerHealth } as model) =
  case
    msg
  of
    Message.Noop ->
      Update.NoChange Cmd.none

    Message.Exit ->
      Update.Exit

    Message.TimePasses newTime ->
      Animation.update newTime model

    Message.Move dir ->
      case
        Player.update levelBox dir model
      of
        PlayerUpdate.Exit ->
          Update.NoChange Cmd.none

        PlayerUpdate.NoTurn TurnType.Move ->
          Player.FailedMovement dir
            |> animationsCommand Dict.empty [] Nothing
            |> Update.NoChange

        PlayerUpdate.NoTurn TurnType.Attack ->
          Player.FailedMovement dir
            |> animationsCommand Dict.empty [] (Just model.level.playerLocation)
            |> Update.NoChange

        PlayerUpdate.TookTurn turnType newModel ->
          if
            newModel.level.playerLocation == newModel.level.exitLocation
          then
            Player.Movement dir
              |> animationsCommand Dict.empty [] Nothing
              |> Update.InternalChange
                { newModel
                | turnsLived =
                  turnsLived + 1
                , floorsDescended =
                  floorsDescended + 1
                }
          else
            let
              ( finalModel, monsterAnimations, damage ) =
                Level.nextTurn levelBox newModel

              cmd =
                animationsCommand monsterAnimations damage Nothing <|
                  case
                    turnType
                  of
                    TurnType.Attack ->
                      Player.Attack dir

                    TurnType.Move ->
                      Player.Movement dir
            in
              if
                not (PlayerHealth.alive finalModel.playerHealth)
                || Level.cornered levelBox finalModel
              then
                Update.Defeat
                  { finalModel
                  | turnsLived =
                    turnsLived + 1
                  }
              else
                Update.InternalChange
                  { finalModel
                  | turnsLived =
                    turnsLived + 1
                  }
                  cmd

    Message.AddAnimations { currentTime, playerAnimation, monsterAnimations, damageAnimations, failedAttackAnimation } ->
      Update.InternalChange
        { level =
          { level
          | monsterMap =
            Dict.foldl
              ( \ pos anim ->
                Dict.update
                  pos
                  ( Maybe.map
                    ( \ monster ->
                      { monster
                      | animation =
                        Animated.delayed
                          currentTime
                          75
                          100
                          anim
                          |> Just
                      }
                    )
                  )
              )
              level.monsterMap
              monsterAnimations
          }
        , playerAnimation =
          Animated.immediate
            currentTime
            75
            playerAnimation
            |> Just
        , playerHealth =
          playerHealth
        , damageAnimations =
          List.map (Animated.delayed currentTime 75 150) damageAnimations
            ++ model.damageAnimations
        , failedAttackAnimation =
          case
            failedAttackAnimation
          of
            Nothing ->
              model.failedAttackAnimation

            Just location ->
              Animated.immediate
                currentTime
                75
                location
                |> Just
        , turnsLived =
          turnsLived
        , floorsDescended =
          floorsDescended
        }
        Cmd.none
