module Level.Cornered
  exposing
    ( cornered
    )


import Dict


import Level.Model.Type
  as Level
import Level.Player.Health.Get
  as PlayerHealth
import Level.Player.Update
  as Player
import Level.Player.Update.Result.Type
  as Update
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Direction.Type
  as Direction
import Util.Direction.ToOffset
  as Direction
import Util.Position.Move
  as Position


cornered : Box Int -> Level.Model -> Bool
cornered levelBox model =
  List.all
    ( \ dir ->
      case
        Player.update levelBox dir model
      of
        Update.NoTurn _ ->
          True

        _ ->
          False
    )
    [ Direction.North
    , Direction.South
    , Direction.West
    , Direction.East
    ]
