module Level.Subscriptions
  exposing
    ( subscriptions
    )


import Browser.Events
  as Browser
import Dict
import Time


import Keys
import Level.Message.Type
  exposing
    ( Message (..)
    )
import Level.Model.Type
  exposing
    ( Model
    )
import Settings.Type
  exposing
    ( Settings
    )
import Util.Direction.Type
  as Direction


animationSubs : Sub Message
animationSubs =
  Time.posixToMillis >> TimePasses
    |> Browser.onAnimationFrame


keyboardSubs : Settings -> Sub Message
keyboardSubs settings =
  Sub.batch
    [ Direction.North
      |> Move
      |> Dict.singleton settings.controls.playerMoveNorth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.South
      |> Move
      |> Dict.singleton settings.controls.playerMoveSouth
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.East
      |> Move
      |> Dict.singleton settings.controls.playerMoveEast
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    , Direction.West
      |> Move
      |> Dict.singleton settings.controls.playerMoveWest
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    ]




subscriptions : Settings -> Model -> Sub Message
subscriptions settings { damageAnimations, playerAnimation, level } =
  Sub.batch
    [ if
        playerAnimation /= Nothing
          || not (List.isEmpty damageAnimations)
          || Dict.foldl (always (.animation >> (/=) Nothing >> (||))) False level.monsterMap
      then
        animationSubs
      else
        Sub.none
    , if
        level.playerLocation == level.exitLocation
      then
        Sub.none
      else
        keyboardSubs settings
    ]
