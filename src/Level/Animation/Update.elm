module Level.Animation.Update
  exposing
    ( update
    )


import Dict


import Animated.Update
  as Animated
import Level.Model.Type
  as Level
import Level.Player.Health.Finish
  as PlayerHealth
import Level.Update.Result.Type
  as Update


update : Int -> Level.Model -> Update.Result
update newTime ({ level, playerHealth } as model) =
  case
    model.playerAnimation
  of
    Just animation ->
      let
        newAnimation =
          Animated.update newTime animation
      in
        if
          newAnimation == Nothing
            && level.playerLocation == level.exitLocation
        then
          model.damageAnimations
            |> List.filterMap (Animated.update newTime)
            |> Update.Win (PlayerHealth.finish playerHealth)
        else
          Update.InternalChange
            { level =
              { level
              | monsterMap =
                Dict.map
                  ( \ _ m ->
                    { m
                    | animation =
                      Maybe.andThen (Animated.update newTime) m.animation
                    }
                  )
                  level.monsterMap
              }
            , damageAnimations =
              List.filterMap
                ( Animated.update newTime )
                model.damageAnimations
            , playerAnimation =
              newAnimation
            , failedAttackAnimation =
              Maybe.andThen (Animated.update newTime) model.failedAttackAnimation
            , playerHealth =
              playerHealth
            , turnsLived =
              model.turnsLived
            , floorsDescended =
              model.floorsDescended
            }
            Cmd.none

    Nothing ->
      if
        Dict.foldl (always (.animation >> (==) Nothing >> (&&))) True level.monsterMap
      then
        if
          List.isEmpty model.damageAnimations
        then
          case
            model.failedAttackAnimation
          of
            Nothing ->
              Update.NoChange Cmd.none

            Just animation ->
              Update.InternalChange
                { level =
                  level
                , playerAnimation =
                  model.playerAnimation
                , damageAnimations =
                  model.damageAnimations
                , playerHealth =
                  playerHealth
                , failedAttackAnimation =
                  Animated.update newTime animation
                , turnsLived =
                  model.turnsLived
                , floorsDescended =
                  model.floorsDescended
                }
                Cmd.none
        else
          Update.InternalChange
            { level =
              level
            , playerAnimation =
              model.playerAnimation
            , damageAnimations =
              List.filterMap
                ( Animated.update newTime )
                model.damageAnimations
            , playerHealth =
              playerHealth
            , failedAttackAnimation =
              Maybe.andThen (Animated.update newTime) model.failedAttackAnimation
            , turnsLived =
              model.turnsLived
            , floorsDescended =
              model.floorsDescended
            }
            Cmd.none
      else
        Update.InternalChange
          { level =
            { level
            | monsterMap =
              Dict.map
                ( \ _ m ->
                  { m
                  | animation =
                    Maybe.andThen (Animated.update newTime) m.animation
                  }
                )
                level.monsterMap
            }
          , playerAnimation =
            model.playerAnimation
          , damageAnimations =
            List.filterMap
              ( Animated.update newTime )
              model.damageAnimations
          , playerHealth =
            playerHealth
          , failedAttackAnimation =
            Maybe.andThen (Animated.update newTime) model.failedAttackAnimation
          , turnsLived =
            model.turnsLived
          , floorsDescended =
            model.floorsDescended
          }
          Cmd.none
