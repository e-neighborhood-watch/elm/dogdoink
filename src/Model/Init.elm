module Model.Init
  exposing
    ( init
    )

import Level.Status.Type
  as Status
import LevelSelect.Model.Init
  as LevelSelect
import Model.Type
  as Model
  exposing
    ( Model
    )
import Message.Type
  exposing
    ( Message
    )
import Settings.Init
  as Settings


init : a -> ( Model, Cmd Message )
init _ =
  ( { settings =
      Settings.init
    , model =
      Model.LevelSelect LevelSelect.init
    }
  , Cmd.none
  )

