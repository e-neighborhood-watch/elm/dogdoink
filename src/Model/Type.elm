module Model.Type
  exposing
    ( Model
    , InnerModel (..)
    )


import HelpScreen.Model.Type
  as HelpScreen
import Level.Model.Type
  as Level
import Level.Player.Health.Type
  as Player
import LevelSelect.Model.Type
  as LevelSelect
import Model.Suspended.Type
  exposing
    ( SuspendedModel
    )
import Prize.Model.Type
  as Prize
import Settings.Model.Type
  as Settings
import Settings.Type
  exposing
    ( WithSettings
    )
import Util.Position.Type
  exposing
    ( Position
    )


-- the Position Int field of the Level and GeneratingLevel constructors
-- represent the level size
type InnerModel
  = LevelSelect LevelSelect.Model
  | HelpScreen HelpScreen.Model
  | Level Int (Position Int) Level.Model
  | Settings SuspendedModel Settings.Model
  | GeneratingLevel Int (Position Int) Player.Health
  | Prize Int (Position Int) Prize.Model
  | Death Int (Position Int) Level.Model


type alias Model =
  WithSettings
    { model :
      InnerModel
    }
