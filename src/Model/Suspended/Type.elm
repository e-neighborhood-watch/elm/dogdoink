module Model.Suspended.Type
  exposing
    ( SuspendedModel (..)
    )


import Level.Model.Type
  as Level
import LevelSelect.Model.Type
  as LevelSelect
import Prize.Model.Type
  as Prize
import Util.Position.Type
  exposing
    ( Position
    )


type SuspendedModel
  = LevelSelect LevelSelect.Model
  | Level Int (Position Int) Level.Model
  | Prize Int (Position Int) Prize.Model
