module Death.Update.Result.Type
  exposing
    ( Result (..)
    )


import Level.Message.Type
  as Level
import Level.Model.Type
  as Level


type Result
  = NoChange
  | InternalChange Level.Model (Cmd Level.Message)
  | Exit
