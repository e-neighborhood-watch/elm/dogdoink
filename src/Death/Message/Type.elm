module Death.Message.Type
  exposing
    ( Message (..)
    )


type Message
  = PlayAgain
  | Noop
  | TimePasses Int
