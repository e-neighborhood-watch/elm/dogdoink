module Death.Update
  exposing
    ( update
    )


import Death.Message.Type
  as Message
  exposing
    ( Message
    )
import Death.Update.Result.Type
  as Update
import Level.Animation.Update
  as Animation
import Level.Model.Type
  as Level
import Level.Update.Result.Type
  as Level


update : Message -> Level.Model -> Update.Result
update msg model =
  case
    msg
  of
    Message.Noop ->
      Update.NoChange

    Message.TimePasses newTime ->
      case
        Animation.update newTime model
      of
        Level.InternalChange newModel cmd ->
          Update.InternalChange newModel cmd
        _ ->
          Update.NoChange

    Message.PlayAgain ->
      Update.Exit
