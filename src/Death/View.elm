module Death.View
  exposing
    ( view
    )


import Css
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Events
  as Svg
import Svg.Styled.Attributes
  as SvgAttr


import Data.Color.Theme.Type
  as Color
import Data.Text.Type
  as Text
import Death.Message.Type
  as Death
import Document.Type
  exposing
    ( Document
    )
import Level.Model.Type
  as Level
import Level.View
  as Level
import Settings.Color.Theme
  as Theme
import Settings.Color.Theme.Type
  as Theme
import Settings.Font.Css
  as Font
import Settings.Font.Type
  as Font
import Settings.Language
  as Language
import Settings.Type
  exposing
    ( Settings
    )
import Util.Box.Type
  exposing
    ( Box
    )


view : Settings -> Box Int -> Level.Model -> Document Death.Message
view settings levelBox model =
  let
    levelDocument =
      Level.view settings levelBox model

    viewBox =
      levelDocument.viewBox

    title =
      Language.fromText settings.language Text.DeathTitle
  in
    { title =
      title
    , viewBox =
      viewBox
    , body =
      [ Svg.g [] levelDocument.body
        |> Svg.map (always Death.Noop)
      , Svg.text_
        [ SvgAttr.css
          [ Css.property "dominant-baseline" "hanging"
          , Css.property "text-anchor" "middle"
          ]
        , SvgAttr.x ((viewBox.min.x + viewBox.max.x) / 2 |> String.fromFloat)
        , SvgAttr.y (levelBox.max.y |> toFloat |> (+) 0.25 |> String.fromFloat)
        , Font.toCss Font.Title settings.fontSize
        , SvgAttr.fill (Theme.toHex settings.theme Color.Blood)
        ]
        [ Svg.text (Language.fromText settings.language Text.DeathTitle)
        ]
      , Svg.text_
        [ SvgAttr.css
          [ Css.property "dominant-baseline" "hanging"
          , Css.property "text-anchor" "middle"
          ]
        , SvgAttr.x ((viewBox.min.x + viewBox.max.x) / 2 |> String.fromFloat)
        , SvgAttr.y (levelBox.max.y |> toFloat |> (+) 0.75 |> String.fromFloat)
        , Font.toCss Font.Text settings.fontSize
        , SvgAttr.fill (Theme.toHex settings.theme Color.Blood)
        ]
        [ Svg.text (Language.fromText settings.language Text.DeathSubtitle)
        ]
      ]
    }
