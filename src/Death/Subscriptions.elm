module Death.Subscriptions
  exposing
    ( subscriptions
    )

import Browser.Events
  as Browser
import Dict
import Time


import Death.Message.Type
  exposing
    ( Message (..)
    )
import Keys
import Level.Model.Type
  as Level
import Settings.Type
  exposing
    ( Settings
    )


animationSubs : Sub Message
animationSubs =
  Time.posixToMillis >> TimePasses
    |> Browser.onAnimationFrame


keyboardSubs : Settings -> Sub Message
keyboardSubs settings =
  Sub.batch
    [ PlayAgain
      |> Dict.singleton settings.controls.levelSelectSelect
      |> Keys.makeKeyDecoder
      |> Browser.onKeyDown
    ]


subscriptions : Settings -> Level.Model -> Sub Message
subscriptions settings { damageAnimations, playerAnimation, level } =
  if
    playerAnimation /= Nothing
      || not (List.isEmpty damageAnimations)
      || Dict.foldl (always (.animation >> (/=) Nothing >> (||))) False level.monsterMap
  then
    Sub.batch
     [ animationSubs
     , keyboardSubs settings
     ]
  else
    keyboardSubs settings
