module Animated.Update
  exposing
    ( update
    )


import Animated.Type
  exposing
    ( Animated
    )


update : Int -> Animated a -> Maybe (Animated a)
update newTime animated =
  if
    newTime >= animated.endingTime
  then
    Nothing
  else
    Just
      { animated
      | currentTime =
        newTime
      }
