module Animated.Delayed
  exposing
    ( delayed
    )


import Animated.Type
  exposing
    ( Animated
    )


delayed : Int -> Int -> Int -> a -> Animated a
delayed currentTime delay duration kind =
  { currentTime =
    currentTime
  , startingTime =
    currentTime + delay
  , endingTime =
    currentTime + delay + duration
  , kind =
    kind
  }
