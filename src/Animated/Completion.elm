module Animated.Completion
  exposing
    ( completion
    )


import Animated.Type
  exposing
    ( Animated
    )


completion : Animated a -> Float
completion { currentTime, startingTime, endingTime } =
  toFloat (endingTime - currentTime) / toFloat (endingTime - startingTime)
    |> clamp 0 1
