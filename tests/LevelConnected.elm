module LevelConnected
  exposing 
    ( suite
    )

import Expect
  exposing
    ( Expectation
    )
import Fuzz
import Test
  exposing
    ( Test
    )
import Shrink


import Set
  exposing
    ( Set
    )


import Level.Walls.Generator
  as Walls
import Level.Size
  as Level
import Level.Walls.Type
  exposing
    ( Walls
    )


isConnectedHelper : List ( Int, Int ) -> Set ( Int, Int ) -> Walls -> Set ( Int, Int )
isConnectedHelper toCheck painted walls =
  case
    toCheck
  of
    [] ->
      painted

    ( x, y ) :: rest ->
      let
        newSpaces =
          List.filterMap
            identity
              [ if
                  y <= 0
                    || Set.member ( x, y ) walls.horizontal
                    || Set.member ( x, y-1 ) painted
                then
                  Nothing
                else
                  Just ( x, y - 1 )
              , if
                  y >= Level.defaultSize.y - 1
                    || Set.member ( x, y+1 ) walls.horizontal
                    || Set.member ( x, y+1 ) painted
                then
                  Nothing
                else
                  Just ( x, y + 1 )
              , if
                  x >= Level.defaultSize.x - 1
                    || Set.member ( x+1, y ) walls.vertical
                    || Set.member ( x+1, y ) painted
                then
                  Nothing
                else
                  Just ( x + 1, y )
              , if
                  x <= 0
                    || Set.member ( x, y ) walls.vertical
                    || Set.member ( x-1, y ) painted
                then
                  Nothing
                else
                  Just ( x - 1, y )
              ]
      in
        isConnectedHelper (newSpaces ++ rest) (Set.insert ( x, y ) painted) walls


isConnected : Walls -> Bool
isConnected =
  isConnectedHelper [( 0, 0 )] Set.empty
    >> Set.size
    >> (==) (Level.defaultSize.x * Level.defaultSize.y)


suite : Test
suite =
  Test.fuzz (Fuzz.custom (Walls.generator Level.defaultSize) Shrink.noShrink) "Testing that walls don't split levels" (isConnected >> Expect.true "expected level to be connected")
