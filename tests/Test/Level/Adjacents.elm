module Test.Level.Adjacents
  exposing
    ( suite
    )


import Set


import Expect
  exposing
    ( Expectation
    )
import Fuzz
import Test
  exposing
    ( Test
    )
import Shrink
  exposing
    ( Shrinker
    )


import Level.Adjacents
  as Level
import Level.Size
  as Level
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.Type
  exposing
    ( Position
    )


defaultBox : Box Int
defaultBox =
  Box (Position 0 0) Level.defaultSize


suite : Test
suite =
  Test.describe
    "Level adjacents"
    [ Test.describe
      "empty level"
      [ Test.describe
        "corners"
        [ Test.test
          "Northwest"
          ( \ _ ->
            Level.adjacents
              defaultBox
              { horizontal =
                Set.empty
              , vertical =
                Set.empty
              }
              defaultBox.min
              |> Expect.equalSets
                ( Set.fromList
                  [ ( defaultBox.min.x, defaultBox.min.y + 1 )
                  , ( defaultBox.min.x + 1, defaultBox.min.y )
                  ]
                )
          )
        ]
      ]
    ]
