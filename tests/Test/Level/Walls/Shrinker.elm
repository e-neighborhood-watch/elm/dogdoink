module Test.Level.Walls.Shrinker
  exposing
    ( shrinker
    )


import Shrink
  exposing
    ( Shrinker
    )


import Set
  exposing
    ( Set
    )


import Level.Walls.Type
  exposing
    ( Walls
    )


setShrinker : Shrinker comparable -> Shrinker (Set comparable)
setShrinker =
  Shrink.list
    >> Shrink.convert Set.fromList Set.toList


shrinker : Shrinker Walls
shrinker { vertical, horizontal } =
  setShrinker Shrink.noShrink vertical
    |> Shrink.map Walls
    |> Shrink.andMap (setShrinker Shrink.noShrink horizontal)
