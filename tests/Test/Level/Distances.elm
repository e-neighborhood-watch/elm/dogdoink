module Test.Level.Distances
  exposing 
    ( suite
    )


import Expect
  exposing
    ( Expectation
    )
import Fuzz
import Test
  exposing
    ( Test
    )
import Shrink
  exposing
    ( Shrinker
    )


import Array
import Dict


import Level.Distances
  as Level
import Level.Size
  as Level
import Level.Walls.Generator
  as Walls
import Level.Walls.Type
  exposing
    ( Walls
    )
import Util.Box.Type
  exposing
    ( Box
    )
import Util.Position.FromIndex
  as Position
import Util.Position.ToPair
  as Position
import Util.Position.Type
  exposing
    ( Position
    )


import Test.Level.Walls.Shrinker
  as Walls


defaultBox : Box Int
defaultBox =
  Box (Position 0 0) Level.defaultSize


suite : Test
suite =
  Test.describe
    "Level distances"
    [ Test.fuzz2
      ( Fuzz.map2 Position (Fuzz.intRange 0 (Level.defaultSize.x - 1)) (Fuzz.intRange 0 (Level.defaultSize.y - 1)) )
      ( Fuzz.custom (Walls.generator Level.defaultSize) Walls.shrinker )
      "All cells should have a distance"
      ( \ position ->
        Level.distances
          defaultBox
          position
          >> Array.toIndexedList
          >> List.map
            ( Tuple.mapFirst (Position.fromIndex defaultBox >> Position.toPair) )
          >> List.filter
            ( Tuple.second >> ((>) 0) )
          >> Dict.fromList
          >> Expect.equalDicts Dict.empty
      )
    ]
